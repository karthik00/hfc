package com.alphadevs.hfc.services.entities;

import com.alphadevs.hfc.commons.models.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Date;
import java.util.List;

public class EventDetails {

    private String description;
    private String name;

	private NGO sponsoringNGO;

	private List<Location> locations;
    private List<Requirement> requirements;
    private List<FieldOfAction> fieldsOfAction;

    private Date date;

    public EventDetails() {
    }

    public EventDetails(String name, String description, Date date, List<Location> locations, List<FieldOfAction> fieldsOfAction, List<Requirement> requirements) {
        this.name = name;
        this.description = description;
        this.date = date;
        this.locations = locations;
        this.fieldsOfAction = fieldsOfAction;
        this.requirements = requirements;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public List<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<Requirement> requirements) {
        this.requirements = requirements;
    }

    public List<FieldOfAction> getFieldsOfAction() {
        return fieldsOfAction;
    }

    public void setFieldsOfAction(List<FieldOfAction> fieldsOfAction) {
        this.fieldsOfAction = fieldsOfAction;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this,o);
	}

	@Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public static EventDetails getEventDetailsFor(Event event) {
        return new EventDetails(event.getName(), event.getDescription(), event.getDate(),
                event.getLocations(),
                event.getFieldsOfAction(),
                event.getRequirements());
    }
}
