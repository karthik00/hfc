package com.alphadevs.hfc.services;

import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.services.entities.SearchCriteria;
import org.springframework.data.domain.Pageable;

public interface NGOSearchService {
    Iterable<NGO> searchNGOMatchingCriteria(SearchCriteria searchCriteria, Pageable pagable);
    Iterable<NGO> searchNGOMatchingCriteria(SearchCriteria searchCriteria);
}
