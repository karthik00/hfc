package com.alphadevs.hfc.services.entities;

import com.alphadevs.hfc.commons.models.FieldOfAction;
import com.alphadevs.hfc.commons.models.Location;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.models.Requirement;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.List;

public class NGODetails {

    private String name;
    private String vision;
    private String mission;
    private String description;

    private List<Location> locations;

    private List<FieldOfAction> fieldsOfAction;

    private List<Requirement> requirements;

    public NGODetails() {
    }

    public NGODetails(String name, String vision, String mission, String description, List<Location> locations, List<FieldOfAction> fieldsOfAction, List<Requirement> requirements) {
        this.name = name;
        this.vision = vision;
        this.mission = mission;
        this.description = description;
        this.locations = locations;
        this.fieldsOfAction = fieldsOfAction;
        this.requirements = requirements;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVision() {
        return vision;
    }

    public void setVision(String vision) {
        this.vision = vision;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public List<FieldOfAction> getFieldsOfAction() {
        return fieldsOfAction;
    }

    public void setFieldsOfAction(List<FieldOfAction> fieldsOfAction) {
        this.fieldsOfAction = fieldsOfAction;
    }

    public List<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<Requirement> requirements) {
        this.requirements = requirements;
    }

    @Override
    public boolean equals(Object ngoDetails) {
        return EqualsBuilder.reflectionEquals(this,ngoDetails);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public static NGODetails getNGODetailsFor(NGO ngo) {
        return new NGODetails(ngo.getName(),ngo.getVision(),ngo.getMission(),ngo.getDescription(),ngo.getLocations(),ngo.getFieldsOfAction(),ngo.getRequirements());
    }
}
