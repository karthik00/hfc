package com.alphadevs.hfc.services;

import com.alphadevs.hfc.commons.models.Event;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.models.UserAccount;
import org.bson.types.ObjectId;

public interface NGOFetchService {
    NGO fetchNGODetailsById(String id);
    NGO fetchNGODetailsById(ObjectId id);
    Iterable<Event> fetchNGOEventsById(ObjectId id);
    Iterable<Event> fetchNGOEventsById(String id);
    Iterable<UserAccount> fetchNGOFollowersById(ObjectId id);
    Iterable<UserAccount> fetchNGOFollowersById(String id);
}
