package com.alphadevs.hfc.services;

import com.alphadevs.hfc.commons.models.Event;
import com.alphadevs.hfc.commons.repositories.EventRepository;
import com.alphadevs.hfc.services.entities.EventDetails;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventFetchService {

    @Autowired
    EventRepository eventRepository;

    public Event fetchEventDetailsById(String id) {
        return eventRepository.findOne(new ObjectId(id));
    }
}
