package com.alphadevs.hfc.services.entities;

public class NGORegistrationDetails {
    private final String name;
    private final String desc;
    private final String vision;
    private final String mission;

    public NGORegistrationDetails(String name, String desc, String vision, String mission) {
        this.name = name;
        this.desc = desc;
        this.vision = vision;
        this.mission = mission;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public String getVision() {
        return vision;
    }

    public String getMission() {
        return mission;
    }
}
