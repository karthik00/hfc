package com.alphadevs.hfc.services;

import com.alphadevs.hfc.commons.models.Event;
import com.alphadevs.hfc.commons.models.FieldOfAction;
import com.alphadevs.hfc.commons.models.Requirement;
import com.alphadevs.hfc.commons.repositories.EventRepository;
import com.alphadevs.hfc.commons.repositories.FieldOfActionRepository;
import com.alphadevs.hfc.commons.repositories.RequirementsRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfigServices {

    @Autowired
    RequirementsRepository requirementsRepository;
    @Autowired
    FieldOfActionRepository fieldOfActionRepository;
    @Autowired
    EventRepository eventRepository;

    public Iterable<Requirement> getAllRequirements(){
        return requirementsRepository.findAll();
    }

    public Requirement saveNewRequirement(Requirement requirement){
        return requirementsRepository.save(requirement);
    }

    public void deleteRequirementWithId(String req_id) {
        requirementsRepository.delete(ObjectId.massageToObjectId(req_id));
    }

    public Requirement getRequirementWithName(String req_name){
        return requirementsRepository.findByName(req_name);
    }

    public Requirement updateRequirement(Requirement requirement) {
        return requirementsRepository.save(requirement);
    }

    public Iterable<FieldOfAction> getAllFieldsOfAction(){
        return fieldOfActionRepository.findAll();
    }

    public FieldOfAction updateFieldOfAction(FieldOfAction fieldOfAction) {
        return fieldOfActionRepository.save(fieldOfAction);
    }

    public FieldOfAction getFieldOfActionWithName(String foa_name) {
        return fieldOfActionRepository.findByName(foa_name);
    }

    public void deleteFieldOfActionWithId(String foa_id) {
        fieldOfActionRepository.delete(ObjectId.massageToObjectId(foa_id));
    }

    public FieldOfAction saveNewFieldOfAction(FieldOfAction fieldOfAction) {
        return fieldOfActionRepository.save(fieldOfAction);
    }

    public Iterable<Event> getAllEvents(){
        return eventRepository.findAll();
    }

    public Event saveNewEvent(Event event){
        return eventRepository.save(event);
    }
}
