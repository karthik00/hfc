package com.alphadevs.hfc.services;

import com.alphadevs.hfc.commons.utils.MongoUtils;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.repositories.NGORepository;
import com.alphadevs.hfc.services.entities.SearchCriteria;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NGOSearchServiceImpl implements NGOSearchService {

	private NGORepository ngoRepository;

	@Autowired
	public NGOSearchServiceImpl(NGORepository ngoRepository) {
		this.ngoRepository=ngoRepository;
	}

	public List<NGO> getNGOs(List<String> fieldsOfActionIds, List<String> requirementsIds) {
		List<ObjectId> FOAIds = MongoUtils.GetObjectIds(fieldsOfActionIds);
		List<ObjectId> reqIds = MongoUtils.GetObjectIds(requirementsIds);
		return ngoRepository.searchNGOsByFieldOfActionsAndRequirements(FOAIds, reqIds);
	}

	public List<NGO> getNGOs(List<String> fieldsOfActionIds, List<String> requirementsIds, int pageNumber, int pageSize) {
		List<ObjectId> FOAIds = MongoUtils.GetObjectIds(fieldsOfActionIds);
		List<ObjectId> reqIds = MongoUtils.GetObjectIds(requirementsIds);
		Page<NGO> ngoPage = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(FOAIds, reqIds, new PageRequest(pageNumber, pageSize));
		return ngoPage.getContent();
	}

    public Iterable<NGO> searchNGOMatchingCriteria(SearchCriteria searchCriteria, Pageable pagable) {
        List<ObjectId> FOAIds = MongoUtils.GetObjectIds(searchCriteria.getFieldsOfActionIds());
        List<ObjectId> reqIds = MongoUtils.GetObjectIds(searchCriteria.getRequirementsIds());
        Page<NGO> ngoPage = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(FOAIds, reqIds, pagable);
        return ngoPage.getContent();
    }

    public Iterable<NGO> searchNGOMatchingCriteria(SearchCriteria searchCriteria) {
        //TODO
        return null;
    }
}