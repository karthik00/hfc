package com.alphadevs.hfc.services;

import com.alphadevs.hfc.commons.models.NGOForApproval;
import com.alphadevs.hfc.commons.repositories.NGOForApprovalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NGOAdministrationServices {

    @Autowired
    NGOForApprovalRepository ngoForApprovalRepository;

    public Iterable<NGOForApproval> getAllPendingNGOs(){
        return ngoForApprovalRepository.findByApprovalState(NGOForApproval.NGOApprovalState.WAITING);
    }

    public NGOForApproval approveNGO(NGOForApproval ngoForApproval){
        //create real NGO Entity
        ngoForApproval.setApprovalState(NGOForApproval.NGOApprovalState.APPROVED);
        return ngoForApprovalRepository.save(ngoForApproval);
    }

    public NGOForApproval rejectNGO(NGOForApproval ngoForApproval){
        ngoForApproval.setApprovalState(NGOForApproval.NGOApprovalState.REJECTED);
        return ngoForApprovalRepository.save(ngoForApproval);
    }

    public NGOForApproval saveNewNGOForApproval(NGOForApproval ngoForApproval){
        return ngoForApprovalRepository.save(ngoForApproval);
    }

    public NGOForApproval updateNGOForApproval(NGOForApproval ngoForApproval){
        return ngoForApprovalRepository.save(ngoForApproval);
    }

    public NGOForApproval getPendingNGOWithName(String ngoName){
        return ngoForApprovalRepository.findByName(ngoName);
    }

}
