package com.alphadevs.hfc.services.auth;

import com.alphadevs.hfc.commons.models.SessionToken;
import com.alphadevs.hfc.commons.models.UserAccount;
import com.alphadevs.hfc.commons.repositories.SessionTokenRepository;
import com.alphadevs.hfc.commons.repositories.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.mongo.MongoUsersConnectionRepository;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.stereotype.Service;
import javax.security.sasl.AuthenticationException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class AuthenticationServiceImpl implements ConnectionSignUp, AuthenticationService{

    private UserAccountRepository userAccountRepository;
    private SessionTokenRepository sessionTokenRepository;
	private ConnectionFactoryLocator connectionFactoryLocator;
    private UsersConnectionRepository connectionRepository;

	@Autowired
    public AuthenticationServiceImpl(
            UserAccountRepository userAccountRepository,
            SessionTokenRepository sessionTokenRepository,
            MongoUsersConnectionRepository usersConnectionRepository, ConnectionFactoryLocator connectionFactoryLocator, UsersConnectionRepository connectionRepository) {
        this.userAccountRepository = userAccountRepository;
        this.sessionTokenRepository = sessionTokenRepository;
        this.connectionFactoryLocator = connectionFactoryLocator;
        this.connectionRepository = connectionRepository;
        usersConnectionRepository.setConnectionSignUp(this);
    }

    public SessionToken loginUserWithEmailAndPassword(String email, String password, String userAgent) throws AuthenticationException {
        SessionToken token;
        UserAccount userAccount = userAccountRepository.findByEmailAndPassword(email, getMd5(password));
        if(null != userAccount){
            token = new SessionToken();
            token.setUserAccount(userAccount);
            token.setUserAgent(userAgent);
            sessionTokenRepository.save(token);
            return token;
        }
        else
            throw new AuthenticationException("Invalid Credentials");
    }

    public void logoutSession(String sessionId) throws AuthenticationException {
        SessionToken token = sessionTokenRepository.findOne(sessionId);
        if(null == token)
            throw new AuthenticationException("session token does not exist");
        token.setStatus(SessionToken.SessionStatus.EXPIRED);
        sessionTokenRepository.save(token);
    }

    public SessionToken loginWithSessionToken(String tokenId) throws AuthenticationException {
        SessionToken token = sessionTokenRepository.findValidSessionById(tokenId);
        if(null == token){
            throw new AuthenticationException("Session Expired...");
        }
        return token;
    }

	public Connection<Facebook> getConnectionForFacebook(String token) {
		OAuth2ConnectionFactory<?> connectionFactory = (OAuth2ConnectionFactory<?>) connectionFactoryLocator.getConnectionFactory(Facebook.class);
		return (Connection<Facebook>) connectionFactory.createConnection(new AccessGrant(token));
	}

    public SessionToken loginWithConnection(Connection<?> connection){
        List<String> userIdsWithConnection = connectionRepository.findUserIdsWithConnection(connection);
        String userId = userIdsWithConnection.get(0);
        UserAccount userAccount = userAccountRepository.findOne(userId);
        SessionToken sessionToken = new SessionToken();
        sessionToken.setUserAccount(userAccount);
        sessionToken.setStatus(SessionToken.SessionStatus.VALID);
        return sessionTokenRepository.save(sessionToken);
    }

    public UserAccount registerUser(UserAccount userAccount){
		userAccount.setPassword(getMd5(userAccount.password()));
        return userAccountRepository.save(userAccount);
    }

	private String getMd5(String input){
		MessageDigest md5 = null;
		try {
			md5 = MessageDigest.getInstance("MD5");
			byte[] digest = md5.digest(input.getBytes());
			return new String(digest,"UTF-8");
		} catch (NoSuchAlgorithmException e) {
			throw new UnsupportedOperationException("Should Never happen");
		}
		catch (UnsupportedEncodingException e) {
			throw new UnsupportedOperationException("Should Never happen");
		}
	}

    public String execute(Connection<?> connection) {
        Object api = connection.getApi();
        if(api instanceof Facebook){
            Facebook fbApi= (Facebook) api;
            FacebookProfile userProfile = fbApi.userOperations().getUserProfile();
            UserAccount userAccount = new UserAccount();
            userAccount.setEmail(userProfile.getEmail());
            userAccount.setStatus(UserAccount.AccountStatus.ENABLED);
            userAccount.setFirstName(userProfile.getFirstName());
            userAccount.setLastName(userProfile.getLastName());
            userAccountRepository.save(userAccount);
            return userAccount.getId();
        }
        return null;
    }
}