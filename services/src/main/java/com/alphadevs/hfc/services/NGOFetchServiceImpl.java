package com.alphadevs.hfc.services;

import com.alphadevs.hfc.commons.models.Event;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.models.UserAccount;
import com.alphadevs.hfc.commons.models.follow.NGOFollowerDetail;
import com.alphadevs.hfc.commons.repositories.EventRepository;
import com.alphadevs.hfc.commons.repositories.NGOFollowerRepository;
import com.alphadevs.hfc.commons.repositories.NGORepository;
import com.alphadevs.hfc.services.entities.NGODetails;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class NGOFetchServiceImpl  implements NGOFetchService{

    private NGORepository repository;
    private NGOFollowerRepository ngoFollowerRepository;
    private EventRepository eventRepository;

    @Autowired
    public NGOFetchServiceImpl(NGORepository repository, NGOFollowerRepository ngoFollowerRepository, EventRepository eventRepository) {
        this.repository = repository;
        this.ngoFollowerRepository = ngoFollowerRepository;
        this.eventRepository = eventRepository;
    }

    public NGO fetchNGODetailsById(String id){
        return fetchNGODetailsById(new ObjectId(id));
    }

    public NGO fetchNGODetailsById(ObjectId id) {
        return repository.findOne(id);
    }

    public Iterable<Event> fetchNGOEventsById(ObjectId id) {
        return eventRepository.findEventsByNGOId(id);
    }

    public Iterable<Event> fetchNGOEventsById(String id) {
        return fetchNGOEventsById(new ObjectId(id));
    }

    public Iterable<UserAccount> fetchNGOFollowersById(ObjectId id) {
        Iterable<NGOFollowerDetail> byFollowedNGOId = ngoFollowerRepository.findByFollowedNGOId(id);
        List<UserAccount> userAccounts=new ArrayList<UserAccount>();
        for (NGOFollowerDetail ngoFollowerDetail : byFollowedNGOId) {
            userAccounts.add(ngoFollowerDetail.getFollowingUser());
        }
        return userAccounts;
    }

    public Iterable<UserAccount> fetchNGOFollowersById(String id) {
        return fetchNGOFollowersById(new ObjectId(id));
    }
}
