package com.alphadevs.hfc.services;

import com.alphadevs.hfc.commons.models.Event;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.repositories.EventRepository;
import com.alphadevs.hfc.commons.repositories.NGORepository;
import com.alphadevs.hfc.commons.utils.MongoUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventSearchService {

	EventRepository eventRepository;

	@Autowired
	public EventSearchService(EventRepository eventRepository) {
		this.eventRepository=eventRepository;
	}

	public EventSearchService() {
	}
	public List<Event> getEvents(List<String> fieldsOfActionIds, List<String> requirementsIds) {
		List<ObjectId> FOAIds = MongoUtils.GetObjectIds(fieldsOfActionIds);
		List<ObjectId> reqIds = MongoUtils.GetObjectIds(requirementsIds);

		return eventRepository.searchEventsByFieldOfActionsAndRequirements(FOAIds, reqIds);
	}

	public List<Event> getEvents(List<String> fieldsOfActionIds, List<String> requirementsIds, int pageNumber, int pageSize) {
		List<ObjectId> FOAIds = MongoUtils.GetObjectIds(fieldsOfActionIds);
		List<ObjectId> reqIds = MongoUtils.GetObjectIds(requirementsIds);

		Page<Event> eventsPage = eventRepository.searchEventsByFieldOfActionsAndRequirements(FOAIds, reqIds, new PageRequest(pageNumber, pageSize));
		return eventsPage.getContent();
	}
}