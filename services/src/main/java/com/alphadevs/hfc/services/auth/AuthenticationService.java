package com.alphadevs.hfc.services.auth;

import com.alphadevs.hfc.commons.models.SessionToken;
import com.alphadevs.hfc.commons.models.UserAccount;
import org.springframework.social.connect.Connection;
import javax.security.sasl.AuthenticationException;

public interface AuthenticationService {
    /**
     * Basic Login
     *
     * login user using Email and password
     *
     * @param email is user email to login
     * @param password is password
     * @param userAgent is user agent using which user logged in
     * @return sessionToken
     * @throws AuthenticationException
     */
    SessionToken loginUserWithEmailAndPassword(String email, String password, String userAgent) throws AuthenticationException;

    /**
     *
     * Login with session token
     *
     * login user with the existing session
     *
     * @param tokenId
     * @return
     * @throws AuthenticationException
     */
    SessionToken loginWithSessionToken(String tokenId) throws AuthenticationException;


    /**
     *
     * Login with social connection
     *
     * login with Social Connection
     *
     * @param connection
     * @return
     */
    SessionToken loginWithConnection(Connection<?> connection);


    /**
     * Logout
     *
     * expires the existing session.
     *
     * @param sessionId
     * @throws AuthenticationException
     */
    void logoutSession(String sessionId) throws AuthenticationException;

    /**
     * Simple Registration
     *
     * Registers user with username password and other details.
     * @param userAccount
     * @return
     */
    UserAccount registerUser(UserAccount userAccount);
}
