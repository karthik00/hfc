package com.alphadevs.hfc.services.entities;

import java.util.List;

public class SearchCriteria {
    private List<String> fieldsOfActionIds;
    private List<String> requirementsIds;

    public SearchCriteria(List<String> fieldsOfActionIds, List<String> requirementsIds) {
        this.fieldsOfActionIds = fieldsOfActionIds;
        this.requirementsIds = requirementsIds;
    }

    public List<String> getFieldsOfActionIds() {
        return fieldsOfActionIds;
    }

    public List<String> getRequirementsIds() {
        return requirementsIds;
    }
}
