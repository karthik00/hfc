package com.alphadevs.hfc.services;

import com.alphadevs.hfc.commons.builders.NGOBuilder;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.repositories.NGORepository;
import org.bson.types.ObjectId;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class NGOSearchServiceTest {

	@Mock
	NGORepository ngoRepository;

	@Before
	public void setUp() throws Exception {
		initMocks(this);
	}

	@Test
	public void shouldTestNGOSearchService() throws Exception {
		NGOSearchServiceImpl ngoSearchService = new NGOSearchServiceImpl(ngoRepository);
		List<String> fieldsOfActionIds = Arrays.asList("50f6ccdb0364e84559f7291f", "50f6ccdb0364e84559f7291e");
		List<String> requirementsIds = Arrays.asList("50f6ccdb0364e84559f7291f", "50f6ccdb0364e84559f7291e");

		List<ObjectId> fieldsOfActionObjectIds = Arrays.asList(new ObjectId("50f6ccdb0364e84559f7291f"), new ObjectId("50f6ccdb0364e84559f7291e"));
		List<ObjectId> requirementObjectIds = Arrays.asList(new ObjectId("50f6ccdb0364e84559f7291f"), new ObjectId("50f6ccdb0364e84559f7291e"));

		List<NGO> ngos = Arrays.asList(new NGOBuilder().withName("ngo1").build(),new NGOBuilder().withName("ngo2").build());

		when(ngoRepository.searchNGOsByFieldOfActionsAndRequirements(fieldsOfActionObjectIds, requirementObjectIds)).thenReturn(ngos);

		List<NGO> searchedNgos = ngoSearchService.getNGOs(fieldsOfActionIds, requirementsIds);

		MatcherAssert.assertThat(searchedNgos,is(ngos));


		when(ngoRepository.searchNGOsByFieldOfActionsAndRequirements(fieldsOfActionObjectIds, requirementObjectIds, new PageRequest(1, 2))).thenReturn(new PageImpl<NGO>(ngos));

		List<NGO> ngoSearchServiceNGOs = ngoSearchService.getNGOs(fieldsOfActionIds, requirementsIds, 1, 2);

		int index=0;
		for (Iterator<NGO> iterator = ngoSearchServiceNGOs.iterator(); iterator.hasNext(); index++) {
			NGO ngo = iterator.next();
			MatcherAssert.assertThat(ngo, is(ngos.get(index)));
		}
	}
}
