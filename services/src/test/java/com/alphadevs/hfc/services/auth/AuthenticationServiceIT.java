package com.alphadevs.hfc.services.auth;

import com.alphadevs.hfc.commons.models.SessionToken;
import com.alphadevs.hfc.commons.models.UserAccount;
import com.alphadevs.hfc.services.SpringIT;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

public class AuthenticationServiceIT extends SpringIT{

    @Autowired
    AuthenticationServiceImpl authenticationService;


    @Autowired
    MongoTemplate mongoTemplate;

    @After
    @Before
    public void cleanDB(){
        mongoTemplate.getCollection(mongoTemplate.getCollectionName(UserAccount.class)).drop();
        mongoTemplate.getCollection(mongoTemplate.getCollectionName(SessionToken.class)).drop();
    }

//    @Test
//    public void testLoginWithFacebookProfile() throws Exception {
//        Facebook fb= new FacebookTemplate("AAADUzZCYqxhsBAEZAJdFEVptAepl3ERZBvsYZB694DFg9DG5jDzZCNOoODOzEDGUSWAsiYplkAwoBS4mSVyHyMRrC5ZAoqq9ZBTY9lzrEZBfnQZDZD");
//        FacebookProfile userProfile = fb.userOperations().getUserProfile();
//        SessionToken sessionToken = authenticationService.loginWithFacebookProfile(userProfile);
//        assertNotNull(sessionToken);
//    }
}
