package com.alphadevs.hfc.services;

import com.alphadevs.hfc.commons.builders.EventBuilder;
import com.alphadevs.hfc.commons.models.Event;
import com.alphadevs.hfc.commons.repositories.EventRepository;
import org.bson.types.ObjectId;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class EventSearchServiceTest {

	@Mock
	EventRepository eventRepository;

	@Before
	public void setUp() throws Exception {
		initMocks(this);
	}

	@Test
	public void shouldTestEventSearchService() throws Exception {
		EventSearchService eventSearchService = new EventSearchService(eventRepository);
		List<String> fieldsOfActionIds = Arrays.asList("50f6ccdb0364e84559f7291f", "50f6ccdb0364e84559f7291e");
		List<String> requirementsIds = Arrays.asList("50f6ccdb0364e84559f7291f", "50f6ccdb0364e84559f7291e");

		List<ObjectId> fieldsOfActionObjectIds = Arrays.asList(new ObjectId("50f6ccdb0364e84559f7291f"), new ObjectId("50f6ccdb0364e84559f7291e"));
		List<ObjectId> requirementObjectIds = Arrays.asList(new ObjectId("50f6ccdb0364e84559f7291f"), new ObjectId("50f6ccdb0364e84559f7291e"));

		List<Event> events = Arrays.asList(new EventBuilder().withName("event1").build(), new EventBuilder().withName("event2").build());

		when(eventRepository.searchEventsByFieldOfActionsAndRequirements(fieldsOfActionObjectIds, requirementObjectIds)).thenReturn(events);

		List<Event> searchServiceEvents = eventSearchService.getEvents(fieldsOfActionIds, requirementsIds);

		MatcherAssert.assertThat(searchServiceEvents, is(events));

		when(eventRepository.searchEventsByFieldOfActionsAndRequirements(fieldsOfActionObjectIds, requirementObjectIds, new PageRequest(1,2))).thenReturn(new PageImpl<Event>(events));

		List<Event> eventSearchServiceEvents = eventSearchService.getEvents(fieldsOfActionIds, requirementsIds, 1, 2);

		int index=0;
		for (Iterator<Event> iterator = eventSearchServiceEvents.iterator(); iterator.hasNext(); index++) {
			Event event = iterator.next();
			MatcherAssert.assertThat(event, is(events.get(index)));
		}
	}
}
