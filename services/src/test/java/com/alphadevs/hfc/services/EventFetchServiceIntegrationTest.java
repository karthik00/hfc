package com.alphadevs.hfc.services;

import com.alphadevs.hfc.commons.builders.EventBuilder;
import com.alphadevs.hfc.commons.builders.LocationBuilder;
import com.alphadevs.hfc.commons.models.Event;
import com.alphadevs.hfc.commons.models.Location;
import com.alphadevs.hfc.commons.repositories.EventRepository;
import com.alphadevs.hfc.commons.repositories.LocationRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:services-test-config.xml"})
public class EventFetchServiceIntegrationTest {

    @Autowired
    EventFetchService eventFetchService;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    LocationRepository locationRepository;

    @Test
    public void shouldFetchEventGivenIdAsString() throws Exception {
        //setup
        Location address = locationRepository.save(new LocationBuilder().withDefaults().withAddress("address").build());
        Location address1 = locationRepository.save(new LocationBuilder().withDefaults().withAddress("address1").build());
        Event build = new EventBuilder().withDefaults().withName("event1").withLocations(Arrays.asList(address, address1)).build();
        Event savedEvent = eventRepository.save(build);

        assertThat(savedEvent, is(eventFetchService.fetchEventDetailsById(savedEvent.getId().toString())));

    }

    @After
    public void tearDown() throws Exception {
        eventRepository.deleteAll();
        locationRepository.deleteAll();
    }
}
