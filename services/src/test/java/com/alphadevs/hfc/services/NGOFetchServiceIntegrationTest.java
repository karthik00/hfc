package com.alphadevs.hfc.services;

import com.alphadevs.hfc.commons.builders.FieldOfActionBuilder;
import com.alphadevs.hfc.commons.builders.NGOBuilder;
import com.alphadevs.hfc.commons.models.FieldOfAction;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.repositories.FieldOfActionRepository;
import com.alphadevs.hfc.commons.repositories.NGORepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:services-test-config.xml"})
public class NGOFetchServiceIntegrationTest {

    @Autowired
    NGOFetchServiceImpl ngoFetchService;

    @Autowired
    NGORepository ngoRepository;

    @Autowired
    FieldOfActionRepository fieldOfActionRepository;

    @Test
    public void shouldFetchNGOGivenIdAsString() throws Exception {
        //setup
        FieldOfAction field1 = new FieldOfActionBuilder().withDefaults().withName("field1").build();
        fieldOfActionRepository.save(field1);
        FieldOfAction field2 = new FieldOfActionBuilder().withDefaults().withName("field2").build();
        fieldOfActionRepository.save(field2);
        NGO savedNGO = new NGOBuilder().withDefaults().withFieldsOfAction(Arrays.asList(field1, field2)).build();
        ngoRepository.save(savedNGO);

        assertThat(savedNGO, is(ngoFetchService.fetchNGODetailsById(savedNGO.getId().toString())));
    }

	@After
	public void tearDown() throws Exception {
		ngoRepository.deleteAll();
		fieldOfActionRepository.deleteAll();

	}
}
