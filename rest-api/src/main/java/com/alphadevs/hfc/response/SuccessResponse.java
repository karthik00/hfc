package com.alphadevs.hfc.response;

public class SuccessResponse extends Response {

    public SuccessResponse() {
        super();
        this.status=ResponseStatus.SUCCESS;
    }

}