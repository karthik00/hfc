package com.alphadevs.hfc.response;

import java.util.HashMap;

public class Response {

    private HashMap<String, Object> data;
    protected ResponseStatus status;

    public Response() {
        this.data = new HashMap<String, Object>();
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public void put(String key, Object value){
        this.data.put(key, value);
    }

    public HashMap<String, Object> getData() {
        return data;
    }
}
