package com.alphadevs.hfc.response;

public enum ResponseStatus {
    SUCCESS, FAILURE
}
