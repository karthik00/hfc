package com.alphadevs.hfc.response;

public class FailureResponse extends Response{

    private String message;

    public FailureResponse(String message) {
        super();
        this.message = message;
        this.status=ResponseStatus.FAILURE;
    }

    public String getMessage() {
        return message;
    }
}
