package com.alphadevs.hfc.controllers;

import com.alphadevs.hfc.commons.models.SessionToken;
import com.alphadevs.hfc.response.FailureResponse;
import com.alphadevs.hfc.response.Response;
import com.alphadevs.hfc.response.SuccessResponse;
import com.alphadevs.hfc.services.auth.AuthenticationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.security.sasl.AuthenticationException;

@Controller
public class AuthenticationController {

    public AuthenticationServiceImpl authenticationService;

	public AuthenticationController() {
	}

	@Autowired
	public AuthenticationController(AuthenticationServiceImpl authenticationService) {
		this.authenticationService = authenticationService;
	}

	@RequestMapping(value = "/loginWithToken", method = RequestMethod.POST)
    public @ResponseBody
    Response login(@RequestParam(required = true) String token){
        try {
            SessionToken sessionToken = authenticationService.loginWithSessionToken(token);
            SuccessResponse successResponse = new SuccessResponse();
            successResponse.put("auth_token", sessionToken.getId());
            successResponse.put("user", sessionToken.getUserAccount());
            return successResponse;
        } catch (AuthenticationException e) {
            return new FailureResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/loginWithFB", method = RequestMethod.POST)
    public @ResponseBody
    Response loginWithFB(@RequestParam(required = true) String token){
			Connection<Facebook> connection = authenticationService.getConnectionForFacebook(token);
            SessionToken sessionToken = authenticationService.loginWithConnection(connection);
            SuccessResponse successResponse = new SuccessResponse();
            successResponse.put("auth_token", sessionToken.getId());
            successResponse.put("user", sessionToken.getUserAccount());
            return successResponse;
    }



	@RequestMapping(value = "/login", method = RequestMethod.POST)
    public @ResponseBody Response login(@RequestParam(required = true) String email, @RequestParam(required = true) String password){
        try {
            SessionToken sessionToken = authenticationService.loginUserWithEmailAndPassword(email, password, "REST");
            SuccessResponse successResponse = new SuccessResponse();
            successResponse.put("auth_token", sessionToken.getId());
            successResponse.put("user", sessionToken.getUserAccount());
            return successResponse;
        } catch (AuthenticationException e) {
            return new FailureResponse(e.getMessage());
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.DELETE)
    public @ResponseBody Response logout(@RequestParam String token){
        try {
            authenticationService.logoutSession(token);
            return new SuccessResponse();
        } catch (AuthenticationException e) {
            return new FailureResponse(e.getMessage());
        }
    }



}
