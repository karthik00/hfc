package com.alphadevs.hfc.controllers;

import com.alphadevs.hfc.commons.models.Event;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.services.EventFetchService;
import com.alphadevs.hfc.services.NGOFetchServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/fetch")
@Controller
public class EventOrNGOFetchController {

    @Autowired
    NGOFetchServiceImpl ngoFetchService;

    @Autowired
    EventFetchService eventFetchService;

    @RequestMapping("/ngo")
    @ResponseBody
    public NGO getNGODetailsFor(@RequestParam(required = true) String id){
        return ngoFetchService.fetchNGODetailsById(id);
    }

    @RequestMapping("/event")
    @ResponseBody
    public Event getEventDetailsFor(@RequestParam(required = true)String id){
        return eventFetchService.fetchEventDetailsById(id);
    }

}
