package com.alphadevs.hfc.controllers;

import com.alphadevs.hfc.commons.models.Event;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.services.EventSearchService;
import com.alphadevs.hfc.services.NGOSearchServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/search")
public class SearchController {

	private NGOSearchServiceImpl ngoSearchService;

	private EventSearchService eventSearchService;

	public SearchController() {
	}

	@Autowired
	public SearchController(NGOSearchServiceImpl ngoSearchService,EventSearchService eventSearchService) {
		this.ngoSearchService = ngoSearchService;
		this.eventSearchService = eventSearchService;
	}

	@RequestMapping(value = "/ngo", method = RequestMethod.POST, headers = "Content-Type=application/json")
	@ResponseBody
	public List<NGO> searchNGOs(@RequestBody SearchCriteria searchCriteria) {
		int pageNumber = searchCriteria.getPageNumber();
		int pageSize = searchCriteria.getPageSize();
		List<String> fieldsOfActionIds = searchCriteria.getFieldsOfActionIds();
		List<String> requirementsIds = searchCriteria.getRequirementsIds();

		if (pageNumber > 0 && pageSize > 0) {
			return ngoSearchService.getNGOs(fieldsOfActionIds, requirementsIds, pageNumber, pageSize);
		}

		return ngoSearchService.getNGOs(fieldsOfActionIds, requirementsIds);
	}

	@RequestMapping(value = "/event", method = RequestMethod.POST, headers = "Content-Type=application/json")

	@ResponseBody
    public List<Event> searchEvents(@RequestBody SearchCriteria searchCriteria) {
		int pageNumber = searchCriteria.getPageNumber();
		int pageSize = searchCriteria.getPageSize();
		List<String> fieldsOfActionIds = searchCriteria.getFieldsOfActionIds();
		List<String> requirementsIds = searchCriteria.getRequirementsIds();

		if (pageNumber > 0 && pageSize > 0) {
			return eventSearchService.getEvents(fieldsOfActionIds, requirementsIds, pageNumber, pageSize);
		}

		return eventSearchService.getEvents(fieldsOfActionIds, requirementsIds);
	}

}
