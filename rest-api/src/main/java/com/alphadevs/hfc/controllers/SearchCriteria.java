package com.alphadevs.hfc.controllers;

import java.util.List;

public class SearchCriteria {

    private List<String> requirementsIds;

	private List<String> fieldsOfActionIds;

	private int pageNumber;
	private int pageSize;

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

    public List<String> getFieldsOfActionIds() {
        return fieldsOfActionIds;
    }

    public List<String> getRequirementsIds() {
        return requirementsIds;
    }

    public void setRequirementsIds(List<String> requirementsIds) {
        this.requirementsIds = requirementsIds;
    }

    public void setFieldsOfActionIds(List<String> fieldsOfActionIds) {
        this.fieldsOfActionIds = fieldsOfActionIds;
    }
}
