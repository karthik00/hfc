package com.alphadevs.hfc.controllers;

import com.alphadevs.hfc.commons.models.FieldOfAction;
import com.alphadevs.hfc.commons.models.Requirement;
import com.alphadevs.hfc.response.FailureResponse;
import com.alphadevs.hfc.response.Response;
import com.alphadevs.hfc.response.SuccessResponse;
import com.alphadevs.hfc.services.ConfigServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * User: sreehari
 * Date: 27/01/13
 * Time: 09:18
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/config")
public class ConfigurationController {

    @Autowired
    private ConfigServices configServices;

    @RequestMapping(method = RequestMethod.GET, value = "/requirements")
    @ResponseBody
    public Response getAllRequirements() {
        try {
            Iterable<Requirement> allRequirements = configServices.getAllRequirements();
            SuccessResponse successResponse = new SuccessResponse();
            successResponse.put("requirements", allRequirements);
            return successResponse;
        } catch (Exception e) {
            return new FailureResponse(e.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/fieldsofaction")
    @ResponseBody
    public Response getAllFieldsOfAction() {
        try {
            Iterable<FieldOfAction> allFieldsOfAction = configServices.getAllFieldsOfAction();
            SuccessResponse successResponse = new SuccessResponse();
            successResponse.put("fieldsofactions", allFieldsOfAction);
            return successResponse;
        } catch (Exception e) {
            return new FailureResponse(e.getMessage());
        }
    }
}
