package com.alphadevs.hfc.controllers;

import com.alphadevs.hfc.commons.models.SessionToken;
import com.alphadevs.hfc.services.auth.AuthenticationServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created with IntelliJ IDEA.
 * User: karthik
 * Date: 19/01/13
 * Time: 12:40 AM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"testconf.xml"})
public class AuthenticationControllerIT  {

    @Autowired
    AuthenticationServiceImpl authenticationService;
    @Autowired
    ConnectionFactoryLocator connectionFactoryLocator;

    @Test
    public void testTestFBAuth() throws Exception {
        OAuth2ConnectionFactory<?> connectionFactory = (OAuth2ConnectionFactory<?>) connectionFactoryLocator.getConnectionFactory(Facebook.class);
        Connection<Facebook> connection = (Connection<Facebook>) connectionFactory.createConnection(new AccessGrant("AAADUzZCYqxhsBAIbquICFR9yZBNKfu1lZAl61moytSD7zdFdKZCVIz3reGCU2kgTKIauHaROc2mdJvQTsyz0uRo5acTZA3IobfCjcojTa5wZDZD"));
        SessionToken sessionToken = authenticationService.loginWithConnection(connection);
    }
}
