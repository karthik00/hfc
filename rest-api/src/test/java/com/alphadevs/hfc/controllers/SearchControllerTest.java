package com.alphadevs.hfc.controllers;

import com.alphadevs.hfc.commons.builders.EventBuilder;
import com.alphadevs.hfc.commons.builders.NGOBuilder;
import com.alphadevs.hfc.commons.models.Event;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.services.EventSearchService;
import com.alphadevs.hfc.services.NGOSearchServiceImpl;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.status;


public class SearchControllerTest {

	@Mock
    NGOSearchServiceImpl ngoSearchService;

	@Mock
	EventSearchService eventSearchService;

	@Before
	public void setUp() throws Exception {
		initMocks(this);
	}

	@Test
	public void shouldTestNGOSearch() throws Exception {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setFieldsOfActionIds(Arrays.asList("id1", "id2"));
		searchCriteria.setRequirementsIds(Arrays.asList("id1", "id2"));
		Gson gson = new Gson();

		NGO ngo1 = new NGOBuilder().withDefaults().build();
		NGO ngo2 = new NGOBuilder().withDefaults().build();
		List<NGO> ngoList = Arrays.asList(ngo1, ngo2);

		when(ngoSearchService.getNGOs(searchCriteria.getFieldsOfActionIds(), searchCriteria.getRequirementsIds()))
				.thenReturn(ngoList);

		SearchController searchController = new SearchController(ngoSearchService,eventSearchService);

		MockMvcBuilders.standaloneSetup(searchController).build()
				.perform(post("/search/ngo").contentType(MediaType.APPLICATION_JSON)
						.body(gson.toJson(searchCriteria).getBytes()))
				.andExpect(status().is(HttpStatus.OK.value()))
				.andExpect(content().string("[{\"id\":null,\"name\":\"NGO\",\"vision\":null,\"mission\":null,\"locations\":null,\"fieldsOfAction\":null,\"requirements\":null,\"description\":\"description\"},{\"id\":null,\"name\":\"NGO\",\"vision\":null,\"mission\":null,\"locations\":null,\"fieldsOfAction\":null,\"requirements\":null,\"description\":\"description\"}]"));
	}

	@Test
	public void shouldTestEventSearch() throws Exception {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setFieldsOfActionIds(Arrays.asList("id1", "id2"));
		searchCriteria.setRequirementsIds(Arrays.asList("id1", "id2"));
		Gson gson = new Gson();

		Event event = new EventBuilder().withDefaults().build();
		Event event1 = new EventBuilder().withDefaults().build();
		List<Event> eventList = Arrays.asList(event, event1);

		when(eventSearchService.getEvents(searchCriteria.getFieldsOfActionIds(), searchCriteria.getRequirementsIds()))
				.thenReturn(eventList);

		SearchController searchController = new SearchController(ngoSearchService,eventSearchService);

		MockMvcBuilders.standaloneSetup(searchController).build()
				.perform(post("/search/event").contentType(MediaType.APPLICATION_JSON)
						.body(gson.toJson(searchCriteria).getBytes()))
				.andExpect(status().is(HttpStatus.OK.value()))
				.andExpect(content().string(("[{\"id\":null,\"description\":\"description\",\"name\":\"event\",\"sponsoringNGO\":null,\"locations\":null,\"requirements\":null,\"fieldsOfAction\":null,\"date\":null},{\"id\":null,\"description\":\"description\",\"name\":\"event\",\"sponsoringNGO\":null,\"locations\":null,\"requirements\":null,\"fieldsOfAction\":null,\"date\":null}]")));
	}
}
