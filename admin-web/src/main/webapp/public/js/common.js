function del_req(id, obj){
    if (confirm("Really delete this requirement?") && !$(obj).hasClass("disabled")){
        $.ajax({
            url: '/delete_requirement/'+id,
            type: 'DELETE',
            success: function(result) {
                $(obj).parent().parent().remove();
            },
            error:function(err){
                $(obj).removeClass("disabled");
            }
        });
        $(obj).addClass("disabled");
    }
}

function del_foa(id, obj){
    if (confirm("Really delete this requirement?") && !$(obj).hasClass("disabled")){
        $.ajax({
            url: '/delete_field_of_action/'+id,
            type: 'DELETE',
            success: function(result) {
                $(obj).parent().parent().remove();
            },
            error:function(err){
                $(obj).removeClass("disabled");
            }
        });
        $(obj).addClass("disabled");
    }
}