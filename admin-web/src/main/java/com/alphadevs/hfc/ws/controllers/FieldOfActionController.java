package com.alphadevs.hfc.ws.controllers;

import com.alphadevs.hfc.commons.models.FieldOfAction;
import com.alphadevs.hfc.services.ConfigServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FieldOfActionController {

    private static final String PREFIX = "fields_of_action/";
    @Autowired
    private ConfigServices configServices;

    @RequestMapping("/fields_of_action")
    public String showFieldsOfAction(ModelMap modelMap){
        modelMap.put("fields_of_action", configServices.getAllFieldsOfAction());
        return PREFIX+"index";
    }

    @RequestMapping(value = "/create_field_of_action", method = RequestMethod.POST)
    public String createNewFieldOfAction(@ModelAttribute FieldOfAction fieldOfAction){
        FieldOfAction savedFieldOfAction= configServices.saveNewFieldOfAction(fieldOfAction);
        if (savedFieldOfAction!=null){
            return "redirect:/fields_of_action";
        }
        else {
            return PREFIX+"new_field_of_action";
        }
    }

    @RequestMapping(value = "/create_field_of_action", method = RequestMethod.GET)
    public String newFieldOfAction(){
        return PREFIX+"new_field_of_action";
    }

    @RequestMapping(value = "/delete_field_of_action/{foa_id}", method = RequestMethod.DELETE)
    public String deleteFieldOfAction(@PathVariable String foa_id){
        configServices.deleteFieldOfActionWithId(foa_id);
        return "redirect:/fields_of_action";
    }


    @RequestMapping("/edit_field_of_action/{foa_name}")
    public String editFieldOfAction(@PathVariable String foa_name, ModelMap modelMap){
        modelMap.put("field_of_action", configServices.getFieldOfActionWithName(foa_name));
        return PREFIX+"edit_field_of_action";
    }

    @RequestMapping(value = "/update_field_of_action", method = RequestMethod.POST)
    public String updateFieldOfAction(@ModelAttribute FieldOfAction fieldOfAction, ModelMap modelMap){
        if (configServices.updateFieldOfAction(fieldOfAction)!=null){
            return "redirect:/edit_field_of_action/"+fieldOfAction.getName();
        }else {
            //TODO return with error
            return "redirect:/edit_field_of_action/"+fieldOfAction.getName();
        }
    }
}
