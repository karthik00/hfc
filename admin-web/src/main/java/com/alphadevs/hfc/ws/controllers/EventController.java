package com.alphadevs.hfc.ws.controllers;

import com.alphadevs.hfc.commons.models.Event;
import com.alphadevs.hfc.services.ConfigServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * UserAccount: sreehari
 * Date: 28/12/12
 * Time: 16:21
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class EventController {
    private static final String PREFIX = "events/";

    @Autowired
    private ConfigServices configServices;

    @RequestMapping("/events")
    public String listEvents(ModelMap modelMap){
       modelMap.put("events", configServices.getAllEvents());
       return PREFIX+"index";
    }

    @RequestMapping(value="/create_event",method = RequestMethod.GET)
    public String createEvent(){
        return PREFIX+"new_event";
    }
    @RequestMapping(value="/create_event",method = RequestMethod.POST)
    public String createEvent(@ModelAttribute Event event){

        Event savedEvent= configServices.saveNewEvent(event);
        if (savedEvent!=null){
            return "redirect:/events";
        }
        else {
            return PREFIX+"new_event";
        }
    }
}
