package com.alphadevs.hfc.ws.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainAppController {
    @RequestMapping("/")
    public String index(){
        return "index";
    }
}