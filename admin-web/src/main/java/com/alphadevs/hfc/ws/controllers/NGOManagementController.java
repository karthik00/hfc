package com.alphadevs.hfc.ws.controllers;

import com.alphadevs.hfc.commons.models.NGOForApproval;
import com.alphadevs.hfc.services.NGOAdministrationServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class NGOManagementController {

    private static final String PREFIX = "ngos_for_approval/";
    @Autowired
    NGOAdministrationServices administrationServices;

    @RequestMapping("/ngos/pending")
    public String pendingNGOs(ModelMap modelMap){
        Iterable<NGOForApproval> pendingNGOs = administrationServices.getAllPendingNGOs();
        modelMap.put("pending_ngos", pendingNGOs);
        return PREFIX+"pending_ngos";
    }


    @RequestMapping("/ngos/new_application")
    public String newNGORegistration(ModelMap modelMap){
        return PREFIX+"new_application";
    }

    @RequestMapping(value = "/ngos/create_new_ngo_for_approval", method = RequestMethod.POST)
    public String createNewNGOForApproval(@ModelAttribute NGOForApproval ngoForApproval){
        ngoForApproval.setApprovalState(NGOForApproval.NGOApprovalState.WAITING);
        NGOForApproval savedNGO = administrationServices.saveNewNGOForApproval(ngoForApproval);
        if (savedNGO!=null){
            return "redirect:/ngos/pending";
        }
        else {
            return "redirect:/ngos/create_new_ngo_for_approval";
        }
    }

}