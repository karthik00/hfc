package com.alphadevs.hfc.ws.controllers;

import com.alphadevs.hfc.commons.models.Requirement;
import com.alphadevs.hfc.services.ConfigServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RequirementsController {

    private static final String PREFIX = "requirements/";
    @Autowired
    ConfigServices configServices;

    @RequestMapping("/requirements")
    public String showRequirements(ModelMap modelMap){
        modelMap.put("requirements", configServices.getAllRequirements());
        return PREFIX+"index";
    }

    @RequestMapping(value = "/create_requirement", method = RequestMethod.POST)
    public String createNewRequirement(@ModelAttribute Requirement requirement){
        Requirement savedRequirement = configServices.saveNewRequirement(requirement);
        if (savedRequirement!=null){
            return "redirect:/requirements";
        }
        else {
            return PREFIX+"new_requirement";
        }
    }

    @RequestMapping(value = "/create_requirement", method = RequestMethod.GET)
    public String newRequirement(){
        return PREFIX+"new_requirement";
    }

    @RequestMapping(value = "/delete_requirement/{req_id}", method = RequestMethod.DELETE)
    public String deleteRequirement(@PathVariable String req_id){
        configServices.deleteRequirementWithId(req_id);
        return "redirect:/requirements";
    }


    @RequestMapping("/edit_requirement/{req_name}")
    public String editRequirement(@PathVariable String req_name, ModelMap modelMap){
        modelMap.put("requirement", configServices.getRequirementWithName(req_name));
        return PREFIX+"edit_requirement";
    }

    @RequestMapping(value = "/update_requirement", method = RequestMethod.POST)
    public String updateRequirement(@ModelAttribute Requirement requirement, ModelMap modelMap){
        if (configServices.updateRequirement(requirement)!=null){
            return "redirect:/edit_requirement/"+requirement.getName();
        }else {
            //TODO return with error
            return "redirect:/edit_requirement/"+requirement.getName();
        }
    }
}
