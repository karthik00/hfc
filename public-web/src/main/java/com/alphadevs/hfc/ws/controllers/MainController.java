package com.alphadevs.hfc.ws.controllers;

import com.alphadevs.hfc.commons.models.SessionToken;
import com.alphadevs.hfc.ws.controllers.webscopedservices.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class MainController {

    @Autowired
    private SessionService sessionService;

    @RequestMapping(method = RequestMethod.GET)
    public
    String index(ModelMap model){
        SessionToken sessionToken = sessionService.getCurrentSessionToken();
        model.put("sessionToken",sessionToken);
        return "index";
    }
}