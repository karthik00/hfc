package com.alphadevs.hfc.ws.controllers;

import com.alphadevs.hfc.commons.models.UserAccount;
import com.alphadevs.hfc.services.auth.AuthenticationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/register")
public class RegistrationController {

    @Autowired
    AuthenticationServiceImpl authenticationService;

    private static final String VIEWS_ROOT = "registration";

    @RequestMapping(method = RequestMethod.GET)
    public String signup(){
        return VIEWS_ROOT+"/index";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String registerUser(@ModelAttribute UserAccount userAccount){
        authenticationService.registerUser(userAccount);
        return VIEWS_ROOT+"/index";
    }

}
