package com.alphadevs.hfc.ws.controllers.webscopedservices;

import com.alphadevs.hfc.commons.models.SessionToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("service")
public class SessionService {

    @Autowired
    private SessionToken currentSessionToken;

    public SessionToken getCurrentSessionToken() {
        return currentSessionToken;
    }

    public void setCurrentSessionToken(SessionToken currentSessionToken) {
        this.currentSessionToken = currentSessionToken;
    }
}
