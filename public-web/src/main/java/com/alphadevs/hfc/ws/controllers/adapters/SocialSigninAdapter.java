package com.alphadevs.hfc.ws.controllers.adapters;

import com.alphadevs.hfc.commons.models.SessionToken;
import com.alphadevs.hfc.services.auth.AuthenticationServiceImpl;
import com.alphadevs.hfc.ws.controllers.webscopedservices.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.NativeWebRequest;

@Component
public class SocialSigninAdapter implements SignInAdapter{

    @Autowired
    AuthenticationServiceImpl authenticationService;
    @Autowired
    SessionService sessionService;

    public String signIn(String userId, Connection<?> connection, NativeWebRequest request) {
        SessionToken sessionToken = authenticationService.loginWithConnection(connection);
        sessionService.setCurrentSessionToken(sessionToken);
        return "/";
    }
}
