package com.alphadevs.hfc.commons.test;

import com.alphadevs.hfc.commons.SpringIT;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.repositories.NGORepository;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created with IntelliJ IDEA.
 * User: sreehari
 * Date: 02/02/13
 * Time: 00:15
 * To change this template use File | Settings | File Templates.
 */
public class ObservableTest extends SpringIT{
    @Autowired
    NGORepository ngoRepository;

    @Test
    public void testShouldNothing(){
        NGO ngo =  new NGO();
        ngo.setId(new ObjectId());
        ngo.setName("Rape");
        ngo.setMission("Help rapists");

        ngoRepository.save(ngo);
        ngo.setName("Rape1");
        ngo.setMission("Help rapists 1");
        ngoRepository.save(ngo);
    }
}
