package com.alphadevs.hfc.commons.repositories;

import com.alphadevs.hfc.commons.SpringIT;
import com.alphadevs.hfc.commons.models.UserFacebookData;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class UserFacebookDataRepositoryIT extends SpringIT{
    @Autowired
    UserFacebookDataRepository userFacebookDataRepository;


    @Before
    @After
    public void clearDB(){
        userFacebookDataRepository.deleteAll();
    }

    @Test
    public void testShouldFindByFbId() throws Exception {
        UserFacebookData userFacebookData = new UserFacebookData();
        userFacebookData.setFacebookId("xyz");
        userFacebookDataRepository.save(userFacebookData);
        UserFacebookData result = userFacebookDataRepository.findByFacebookId("xyz");
        Assert.assertEquals(result, userFacebookData);
    }
}
