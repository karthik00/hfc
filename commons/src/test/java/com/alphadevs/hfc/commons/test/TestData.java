package com.alphadevs.hfc.commons.test;

import com.alphadevs.hfc.commons.models.FieldOfAction;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.models.Requirement;

public class TestData {
    FieldOfAction fieldOfAction1;
    FieldOfAction fieldOfAction2;
    Requirement requirement1;
    Requirement requirement2;
    NGO ngoWithF1andF2;
    NGO ngoWithF1;
    NGO ngoWithF2;
    NGO ngoWithR1andR2;
    NGO ngoWithR1;
    NGO ngoWithR2;
    NGO ngoWithF1andR1;
    NGO ngoWithF1F2andR1;
    NGO ngoWithF1andR1R2;

    public TestData() {
    }
}