package com.alphadevs.hfc.commons.repositories;


import com.alphadevs.hfc.commons.SpringIT;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.models.UserAccount;
import com.alphadevs.hfc.commons.models.follow.NGOFollowerDetail;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class NGOFollowerIT extends SpringIT{
    @Autowired
    private NGOFollowerRepository ngoFollowerRepository;
    @Autowired
    private UserAccountRepository userAccountRepository;
    @Autowired
    private NGORepository ngoRepository;

    private UserAccount userAccount1;
    private NGO ngo1;
    private UserAccount userAccount2;
    private NGO ngo2;

    @Before
    public void setup(){
        userAccount1 = new UserAccount();
        ngo1 = new NGO();
        userAccount2 = new UserAccount();
        ngo2 = new NGO();
        ngoRepository.save(ngo1);
        ngoRepository.save(ngo2);
        userAccountRepository.save(userAccount1);
        userAccountRepository.save(userAccount2);
    }

    @After
    public void breakDown(){
        userAccountRepository.deleteAll();
        ngoRepository.deleteAll();
        ngoFollowerRepository.deleteAll();
    }


    @Test
    public void testShouldFindUserFollowingNGOs(){
        NGOFollowerDetail ngoFollowerDetail = new NGOFollowerDetail(ngo1,userAccount1);
        ngoFollowerRepository.save(ngoFollowerDetail);
        Iterable<NGOFollowerDetail> userFollowingNGOs = ngoFollowerRepository.findByFollowingUserId(new ObjectId(userAccount1.getId()));
        Iterator<NGOFollowerDetail> iterator = userFollowingNGOs.iterator();
        NGOFollowerDetail followerDetail = iterator.next();
        assertNotNull(followerDetail);
        assertEquals(followerDetail.getFollowingUser(), userAccount1);
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testShouldFindNGOsFollowedByUser(){
        NGOFollowerDetail ngoFollowerDetail = new NGOFollowerDetail(ngo1,userAccount1);
        ngoFollowerRepository.save(ngoFollowerDetail);
        Iterable<NGOFollowerDetail> userFollowingNGOs = ngoFollowerRepository.findByFollowedNGOId(new ObjectId(ngo1.getId()));
        Iterator<NGOFollowerDetail> iterator = userFollowingNGOs.iterator();
        NGOFollowerDetail followerDetail = iterator.next();
        assertNotNull(followerDetail);
        assertEquals(followerDetail.getFollowedNGO(), ngo1);
        assertFalse(iterator.hasNext());
    }
}