package com.alphadevs.hfc.commons.test;

import com.alphadevs.hfc.commons.builders.FieldOfActionBuilder;
import com.alphadevs.hfc.commons.builders.NGOBuilder;
import com.alphadevs.hfc.commons.builders.RequirementBuilder;
import com.alphadevs.hfc.commons.models.FieldOfAction;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.models.Requirement;
import com.alphadevs.hfc.commons.repositories.FieldOfActionRepository;
import com.alphadevs.hfc.commons.repositories.NGORepository;
import com.alphadevs.hfc.commons.repositories.RequirementsRepository;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:commons-dev-config.xml"})
public class NGORepositoryIntegrationTest {

    private final TestData testData = new TestData();
    @Autowired
    NGORepository ngoRepository;

    @Autowired
    FieldOfActionRepository fieldOfActionRepository;

    @Autowired
    RequirementsRepository requirementsRepository;

    @Before
    public void setUp() throws Exception {
        createFieldsOfAction();
        createRequirements();
        createNGOs();
    }

    private void createNGOs() {
        ArrayList<FieldOfAction> fieldsOfAction = new ArrayList<FieldOfAction>() {
            {
                add(testData.fieldOfAction1);
                add(testData.fieldOfAction2);
            }
        };

        testData.ngoWithF1andF2 = new NGOBuilder().withFieldsOfAction(fieldsOfAction).build();
        ngoRepository.save(testData.ngoWithF1andF2);

        fieldsOfAction = new ArrayList<FieldOfAction>() {
            {
                this.add(testData.fieldOfAction1);
            }
        };
        testData.ngoWithF1 = new NGOBuilder().withFieldsOfAction(fieldsOfAction).build();
        ngoRepository.save(testData.ngoWithF1);

        fieldsOfAction = new ArrayList<FieldOfAction>() {
            {
                this.add(testData.fieldOfAction2);
            }
        };
        testData.ngoWithF2 = new NGOBuilder().withFieldsOfAction(fieldsOfAction).build();
        ngoRepository.save(testData.ngoWithF2);

        ArrayList<Requirement> requirements = new ArrayList<Requirement>() {
            {
                this.add(testData.requirement1);
                this.add(testData.requirement2);
            }
        };

        testData.ngoWithR1andR2 = new NGOBuilder().withRequirements(requirements).build();
        ngoRepository.save(testData.ngoWithR1andR2);

        requirements = new ArrayList<Requirement>() {
            {
                this.add(testData.requirement1);
            }
        };
        testData.ngoWithR1 = new NGOBuilder().withRequirements(requirements).build();
        ngoRepository.save(testData.ngoWithR1);

        requirements = new ArrayList<Requirement>() {
            {
                this.add(testData.requirement2);
            }
        };
        testData.ngoWithR2 = new NGOBuilder().withRequirements(requirements).build();
        ngoRepository.save(testData.ngoWithR2);

        fieldsOfAction = new ArrayList<FieldOfAction>() {
            {
                this.add(testData.fieldOfAction1);
            }
        };

        requirements = new ArrayList<Requirement>() {
            {
                this.add(testData.requirement1);
            }
        };

        testData.ngoWithF1andR1 =new NGOBuilder().withRequirements(requirements).withFieldsOfAction(fieldsOfAction).build();
        ngoRepository.save(testData.ngoWithF1andR1);

        fieldsOfAction = new ArrayList<FieldOfAction>() {
            {
                this.add(testData.fieldOfAction1);
                this.add(testData.fieldOfAction2);
            }
        };

        requirements = new ArrayList<Requirement>() {
            {
                this.add(testData.requirement1);
            }
        };

        testData.ngoWithF1F2andR1 = new NGOBuilder().withRequirements(requirements).withFieldsOfAction(fieldsOfAction).build();
        ngoRepository.save(testData.ngoWithF1F2andR1);


        fieldsOfAction = new ArrayList<FieldOfAction>() {
            {
                this.add(testData.fieldOfAction1);
            }
        };

        requirements = new ArrayList<Requirement>() {
            {
                this.add(testData.requirement1);
                this.add(testData.requirement2);
            }
        };

        testData.ngoWithF1andR1R2 = new NGOBuilder().withRequirements(requirements).withFieldsOfAction(fieldsOfAction).build();
        ngoRepository.save(testData.ngoWithF1andR1R2);
    }

    private void createRequirements() {
        testData.requirement1 = new RequirementBuilder().build();
        requirementsRepository.save(testData.requirement1);
        testData.requirement2 = new RequirementBuilder().build();
        requirementsRepository.save(testData.requirement2);
    }

    private void createFieldsOfAction() {
        testData.fieldOfAction1 = new FieldOfActionBuilder().withName("fieldOfAction1").build();
        fieldOfActionRepository.save(testData.fieldOfAction1);
        testData.fieldOfAction2 = new FieldOfActionBuilder().withName("fieldOfAction2").build();
        fieldOfActionRepository.save(testData.fieldOfAction2);
    }

    @After
    public void tearDown() throws Exception {
        fieldOfActionRepository.deleteAll();
        requirementsRepository.deleteAll();
        ngoRepository.deleteAll();
    }

    @Test
    public void testShouldSearchForFieldOfActions(){
        ArrayList<ObjectId> fieldOfActionIds = new ArrayList<ObjectId>() {
            {
                this.add(testData.fieldOfAction1.getId());
            }
        };
        List<NGO> results = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(fieldOfActionIds, new ArrayList<ObjectId>());
        assertTrue(results.size()==5);
        assertTrue(results.contains(testData.ngoWithF1));
        assertTrue(results.contains(testData.ngoWithF1andF2));
        assertTrue(results.contains(testData.ngoWithF1andR1));
        assertTrue(results.contains(testData.ngoWithF1F2andR1));
        assertTrue(results.contains(testData.ngoWithF1andR1R2));

        fieldOfActionIds = new ArrayList<ObjectId>() {
            {
                this.add(testData.fieldOfAction2.getId());
            }
        };
        results = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(fieldOfActionIds, new ArrayList<ObjectId>());
        assertTrue(results.size()==3);
        assertTrue(results.contains(testData.ngoWithF2));
        assertTrue(results.contains(testData.ngoWithF1andF2));
        assertTrue(results.contains(testData.ngoWithF1F2andR1));



        fieldOfActionIds = new ArrayList<ObjectId>() {
            {
                this.add(testData.fieldOfAction1.getId());
                this.add(testData.fieldOfAction2.getId());
            }
        };
        results = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(fieldOfActionIds, new ArrayList<ObjectId>());
        assertTrue(results.size()==6);
        assertTrue(results.contains(testData.ngoWithF2));
        assertTrue(results.contains(testData.ngoWithF1));
        assertTrue(results.contains(testData.ngoWithF1andF2));
        assertTrue(results.contains(testData.ngoWithF1andR1));
        assertTrue(results.contains(testData.ngoWithF1F2andR1));
        assertTrue(results.contains(testData.ngoWithF1andR1R2));
    }

    @Test
    public void testShouldSearchForRequirements(){
        ArrayList<ObjectId> requirementIds = new ArrayList<ObjectId>() {
            {
                this.add(testData.requirement1.getId());
            }
        };
        List<NGO> results = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(new ArrayList<ObjectId>(),requirementIds);
        assertTrue(results.size()==5);
        assertTrue(results.contains(testData.ngoWithR1));
        assertTrue(results.contains(testData.ngoWithR1andR2));
        assertTrue(results.contains(testData.ngoWithF1andR1));
        assertTrue(results.contains(testData.ngoWithF1F2andR1));
        assertTrue(results.contains(testData.ngoWithF1andR1R2));

        requirementIds = new ArrayList<ObjectId>() {
            {
                this.add(testData.requirement2.getId());
            }
        };
        results = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(new ArrayList<ObjectId>(), requirementIds);
        assertTrue(results.size()==3);
        assertTrue(results.contains(testData.ngoWithR2));
        assertTrue(results.contains(testData.ngoWithR1andR2));
        assertTrue(results.contains(testData.ngoWithF1andR1R2));



        requirementIds = new ArrayList<ObjectId>() {
            {
                this.add(testData.requirement1.getId());
                this.add(testData.requirement2.getId());
            }
        };
        results = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(new ArrayList<ObjectId>(), requirementIds);
        assertTrue(results.size()==6);
        assertTrue(results.contains(testData.ngoWithR2));
        assertTrue(results.contains(testData.ngoWithR1));
        assertTrue(results.contains(testData.ngoWithR1andR2));
        assertTrue(results.contains(testData.ngoWithF1andR1));
        assertTrue(results.contains(testData.ngoWithF1F2andR1));
        assertTrue(results.contains(testData.ngoWithF1andR1R2));
    }

    @Test
    public void testShouldSearchForRequirementsAndFieldOfActions(){

        ArrayList<ObjectId> feildOfActionIds = new ArrayList<ObjectId>() {
            {
                this.add(testData.fieldOfAction1.getId());
                this.add(testData.fieldOfAction2.getId());
            }
        };

        ArrayList<ObjectId> requirementIds = new ArrayList<ObjectId>() {
            {
                this.add(testData.requirement1.getId());
                this.add(testData.requirement2.getId());
            }
        };

        List<NGO> results = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(feildOfActionIds,requirementIds);
        assertTrue(results.size()==9);

        feildOfActionIds = new ArrayList<ObjectId>() {
            {
                this.add(testData.fieldOfAction1.getId());
            }
        };
        requirementIds = new ArrayList<ObjectId>() {
            {
                this.add(testData.requirement1.getId());
            }
        };

        results = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(feildOfActionIds, requirementIds);
        assertTrue(results.size()==7);
        assertTrue(results.contains(testData.ngoWithF1));
        assertTrue(results.contains(testData.ngoWithF1andR1));
        assertTrue(results.contains(testData.ngoWithF1andR1R2));
        assertTrue(results.contains(testData.ngoWithF1andF2));
        assertTrue(results.contains(testData.ngoWithR1));
        assertTrue(results.contains(testData.ngoWithR1andR2));
        assertTrue(results.contains(testData.ngoWithF1F2andR1));


        feildOfActionIds = new ArrayList<ObjectId>() {
            {
                this.add(testData.fieldOfAction2.getId());
            }
        };
        requirementIds = new ArrayList<ObjectId>() {
            {
                this.add(testData.requirement1.getId());
            }
        };
        results = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(feildOfActionIds, requirementIds);
        assertTrue(results.size()==7);
        assertTrue(results.contains(testData.ngoWithF1F2andR1));
        assertTrue(results.contains(testData.ngoWithF1andF2));
        assertTrue(results.contains(testData.ngoWithF2));
        assertTrue(results.contains(testData.ngoWithR1));
        assertTrue(results.contains(testData.ngoWithF1andR1R2));
        assertTrue(results.contains(testData.ngoWithR1andR2));
        assertTrue(results.contains(testData.ngoWithF1andR1));
    }


    @Test
    public void testShouldFetchPages(){
        ArrayList<ObjectId> feildOfActionIds = new ArrayList<ObjectId>() {
            {
                this.add(testData.fieldOfAction2.getId());
            }
        };
        ArrayList<ObjectId> requirementIds= new ArrayList<ObjectId>() {
            {
                this.add(testData.requirement1.getId());
            }
        };
        Page<NGO> results = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(feildOfActionIds, requirementIds, new PageRequest(0, 3));
        assertTrue(results.getContent().size()==3);
        assertTrue(results.hasNextPage());
        results = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(feildOfActionIds, requirementIds, new PageRequest(1, 3));
        assertTrue(results.getContent().size()==3);
        assertTrue(results.hasNextPage());
        assertTrue(results.hasPreviousPage());
        results = ngoRepository.searchNGOsByFieldOfActionsAndRequirements(feildOfActionIds, requirementIds, new PageRequest(2, 3));
        assertTrue(results.getContent().size()==1);
        assertFalse(results.hasNextPage());
        assertTrue(results.hasPreviousPage());
    }


}