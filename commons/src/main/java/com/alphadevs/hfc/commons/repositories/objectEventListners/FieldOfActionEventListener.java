package com.alphadevs.hfc.commons.repositories.objectEventListners;

import com.alphadevs.hfc.commons.models.FieldOfAction;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class FieldOfActionEventListener extends AbstractMongoEventListener<FieldOfAction>{
    @Override
    public void onBeforeConvert(FieldOfAction source) {
        if (source.getCreationDate()==null)
            source.setCreationDate(new Date());
        else
            source.setLastUpdatedDate(new Date());
        super.onBeforeConvert(source);
    }
}

