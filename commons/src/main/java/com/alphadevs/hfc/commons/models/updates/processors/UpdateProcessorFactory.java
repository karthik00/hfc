package com.alphadevs.hfc.commons.models.updates.processors;

import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.models.updates.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sreehari
 * Date: 04/02/13
 * Time: 21:49
 * To change this template us   e File | Settings | File Templates.
 */
@Component
public class UpdateProcessorFactory {
    @Autowired
    NGOUpdateProcessor ngoUpdateProcessor;
    public List<UpdateProcessor<NGO>> getNGOUpdateProcessors(){
        List<UpdateProcessor<NGO>> processors= new ArrayList<UpdateProcessor<NGO>>();
        processors.add(ngoUpdateProcessor);
        return processors;
    }

    public List<UpdateProcessor<Event>> getEventUpdateProcessors(){
        List<UpdateProcessor<Event>> processors= new ArrayList<UpdateProcessor<Event>>();
        return processors;
    }
}
