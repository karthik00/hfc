package com.alphadevs.hfc.commons.models.updates;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: sreehari
 * Date: 01/02/13
 * Time: 21:51
 * To change this template use File | Settings | File Templates.
 */
public class Event {
    public Date timestamp;
    public String attributeName;
    public Object attributeValue;

    public Event(String attributeName,Object attributeValue){
      this.timestamp=new Date();
      this.attributeName=attributeName;
      this.attributeValue=attributeValue;
    }
    public Object getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public Object getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(Object attributeValue) {
        this.attributeValue = attributeValue;
    }
}
