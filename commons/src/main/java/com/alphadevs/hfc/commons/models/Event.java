package com.alphadevs.hfc.commons.models;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;


@Document
public class Event {

    @Id
    private ObjectId id;
    private String description;
    private String name;

	@DBRef
    NGO sponsoringNGO;
    @DBRef
    List<Location> locations;
    @DBRef
    List<Requirement> requirements;
    @DBRef
    List<FieldOfAction> fieldsOfAction;
    Date date;

    public Event(){
    }

    Event(String description){
      this.description=description;
    }

	public NGO getSponsoringNGO() {
		return sponsoringNGO;
	}

	public void setSponsoringNGO(NGO sponsoringNGO) {
		this.sponsoringNGO = sponsoringNGO;
	}

	public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name=name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    public List<FieldOfAction> getFieldsOfAction() {
        return fieldsOfAction;
    }

    public void setFieldsOfAction(List<FieldOfAction> fieldsOfAction) {
        this.fieldsOfAction = fieldsOfAction;
    }

    public List<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<Requirement> requirements) {
        this.requirements = requirements;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this,obj);
	}
}
