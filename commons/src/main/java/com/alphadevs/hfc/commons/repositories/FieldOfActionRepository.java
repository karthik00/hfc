package com.alphadevs.hfc.commons.repositories;

import com.alphadevs.hfc.commons.models.FieldOfAction;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FieldOfActionRepository extends MongoRepository<FieldOfAction, ObjectId>{
    FieldOfAction findByName(String name);
}