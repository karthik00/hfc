package com.alphadevs.hfc.commons.models.follow;

import com.alphadevs.hfc.commons.models.UserAccount;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class FollowerDetail {

    @DBRef
    private UserAccount followingUser;
    private SubscriptionDetails subscriptionDetails;

    public FollowerDetail(UserAccount followingUser) {
        this.followingUser = followingUser;
        this.subscriptionDetails=new SubscriptionDetails();
    }

    public SubscriptionDetails getSubscriptionDetails() {
        return subscriptionDetails;
    }

    public void setSubscriptionDetails(SubscriptionDetails subscriptionDetails) {
        this.subscriptionDetails = subscriptionDetails;
    }

    public UserAccount getFollowingUser() {
        return followingUser;
    }

    public void setFollowingUser(UserAccount followingUser) {
        this.followingUser = followingUser;
    }
}
