package com.alphadevs.hfc.commons.builders;

import com.alphadevs.hfc.commons.models.Requirement;

public class RequirementBuilder {

    private Requirement requirement= new Requirement();

    public RequirementBuilder withDefaults(){
        requirement.setName("requirement");
        requirement.setDescription("description");
        return this;
    }

    public RequirementBuilder withName(String name){
        requirement.setName(name);
        return this;
    }

    public RequirementBuilder withDescription(String description){
        requirement.setName(description);
        return this;
    }

    public Requirement build(){
        return requirement;
    }
}
