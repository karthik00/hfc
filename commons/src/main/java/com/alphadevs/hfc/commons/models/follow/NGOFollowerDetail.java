package com.alphadevs.hfc.commons.models.follow;

import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.models.UserAccount;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class NGOFollowerDetail extends FollowerDetail {

    @Id
    private ObjectId id;

    @DBRef
    private NGO followedNGO;

    public NGOFollowerDetail(NGO followedNGO, UserAccount followingUser){
      super(followingUser);
      this.followedNGO = followedNGO;
    }

    public NGO getFollowedNGO() {
        return followedNGO;
    }

    public void setFollowedNGO(NGO followedNGO) {
        this.followedNGO = followedNGO;
    }

}
