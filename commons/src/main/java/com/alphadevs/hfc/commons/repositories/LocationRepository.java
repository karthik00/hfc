package com.alphadevs.hfc.commons.repositories;

import com.alphadevs.hfc.commons.models.Location;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends MongoRepository<Location,ObjectId>{
}
