package com.alphadevs.hfc.commons.repositories;

import com.alphadevs.hfc.commons.models.updates.NGOUpdate;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA.
 * User: sreehari
 * Date: 05/02/13
 * Time: 22:05
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface NGOUpdateRepository extends MongoRepository<NGOUpdate,ObjectId> {
}
