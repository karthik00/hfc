package com.alphadevs.hfc.commons.models;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class NGOForApproval {

    private Date creationDate;
    private Date lastUpdatedDate;

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public static enum NGOApprovalState {
        REJECTED, APPROVED, WAITING
    }

    @Id
    private String id;
    private String name;
    private String vision;
    private String mission;
    private String description;
    private String phoneNo;
    private String contactName;
    private String contactEmail;
    private String websiteURL;
    private NGOApprovalState approvalState;
    private String remarks;

    public NGOApprovalState getApprovalState() {
        return approvalState;
    }

    public void setApprovalState(NGOApprovalState approvalState) {
        this.approvalState = approvalState;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVision() {
        return vision;
    }

    public void setVision(String vision) {
        this.vision = vision;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public String getCreatedPrettyTime(){
        return new PrettyTime().format(this.creationDate);
    }

    public String getUpdatedPrettyTime(){
        return new PrettyTime().format(this.creationDate);
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this).
                append("id", id).
                append("name", name).
                append("vision", vision).
                append("mission", mission).
                append("description", description).
                append("phoneNo", phoneNo).
                append("contactName", contactName).
                append("contactEmail", contactEmail).
                append("websiteURL", websiteURL).
                append("approvalState", approvalState).
                append("remarks", remarks).
                toString();
    }
}
