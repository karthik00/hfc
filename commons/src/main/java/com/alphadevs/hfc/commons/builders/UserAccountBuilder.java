package com.alphadevs.hfc.commons.builders;

import com.alphadevs.hfc.commons.models.UserAccount;
import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: sravank
 * Date: 24/01/13
 * Time: 8:43 AM
 */
public class UserAccountBuilder {

	UserAccount userAccount = new UserAccount();

	public UserAccountBuilder withDefaults() {
        userAccount.setId(new ObjectId());
		userAccount.setEmail("email");
		userAccount.setFbId("fbId");
		userAccount.setFirstName("firstName");
		userAccount.setLastName("lastName");
		userAccount.setMiddleName("middleName");
		userAccount.setPassword("password");
		userAccount.setStatus(UserAccount.AccountStatus.ENABLED);
		userAccount.setCreationDate(new Date());
		userAccount.setLastLoggedInDate(new Date());
		return this;
	}

	public UserAccountBuilder withEmail(String email) {
		userAccount.setEmail(email);
		return this;
	}

	public UserAccountBuilder withFbId(String fbId) {
		userAccount.setFbId(fbId);
		return this;
	}

	public UserAccountBuilder withFirstName(String firstName) {
		userAccount.setFirstName(firstName);
		return this;
	}

	public UserAccount build() {
		return userAccount;
	}
}
