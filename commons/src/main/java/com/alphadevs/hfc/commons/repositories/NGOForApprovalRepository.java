package com.alphadevs.hfc.commons.repositories;


import com.alphadevs.hfc.commons.models.NGOForApproval;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NGOForApprovalRepository extends MongoRepository<NGOForApproval, ObjectId> {
    Iterable<NGOForApproval> findByApprovalState(NGOForApproval.NGOApprovalState state);
    NGOForApproval findByName(String name);
}