package com.alphadevs.hfc.commons.repositories;

import com.alphadevs.hfc.commons.models.Requirement;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequirementsRepository extends MongoRepository<Requirement, ObjectId>{
    Requirement findByName(String name);
}