package com.alphadevs.hfc.commons.repositories.objectEventListners;

import com.alphadevs.hfc.commons.models.Requirement;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class RequirementEventListener extends AbstractMongoEventListener<Requirement>{
    @Override
    public void onBeforeConvert(Requirement source) {
        if (source.getCreationDate()==null)
            source.setCreationDate(new Date());
        else
            source.setLastUpdatedDate(new Date());
        super.onBeforeConvert(source);
    }
}
