package com.alphadevs.hfc.commons.models.updates.processors;

import com.alphadevs.hfc.commons.models.updates.Event;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.models.updates.NGOUpdate;
import com.alphadevs.hfc.commons.models.updates.ObservedField;
import com.alphadevs.hfc.commons.repositories.NGORepository;
import com.alphadevs.hfc.commons.repositories.NGOUpdateRepository;
import com.mongodb.DBObject;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import com.alphadevs.hfc.commons.annotations.*;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sreehari
 * Date: 04/02/13
 * Time: 21:41
 * To change this template use File | Settings | File Templates.
 */
@Component
public class NGOUpdateProcessor implements UpdateProcessor<NGO> {
    @Autowired
    NGORepository ngoRepository;
    @Autowired
    AnnotationParser annotationParser;
    @Autowired
    NGOUpdateRepository ngoUpdateRepository;

    public void processBeforeSaveEvent(NGO source, DBObject dbObject) {
        Class<ObservedField> observedAnnotation = ObservedField.class;
        List<Field> annotatedFields = annotationParser.getAnnotatedFields(NGO.class, observedAnnotation);

        NGO originalNGO = ngoRepository.findOne(new ObjectId(source.getId()));
        if(originalNGO == null)
            return;

        for(Field field : annotatedFields){
                NGOUpdate ngoUpdate = createUpdate(originalNGO, source, field);
                if (ngoUpdate!=null)
                    ngoUpdateRepository.save(ngoUpdate);
        }
    }

    private NGOUpdate createUpdate(NGO originalNGO, NGO source, Field field) {
        try {
            Object originalValue = field.get(originalNGO);
            Object newValue = field.get(source);

            if((originalValue==null && newValue==null) || newValue.equals(originalValue))
                return null;

            if(originalValue == null && newValue !=null){
                Event event = new Event(field.getName(),newValue);
                return new NGOUpdate(originalNGO, event,"ADD");
            }
            if(originalValue != null && newValue !=null){
                Event event = new Event(field.getName(),newValue);
                return new NGOUpdate(originalNGO, event,"CHANGE");
            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
