package com.alphadevs.hfc.commons.annotations;

import com.alphadevs.hfc.commons.models.updates.ObservedField;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sreehari
 * Date: 04/02/13
 * Time: 19:57
 * To change this template use File | Settings | File Templates.
 */
@Component
public class AnnotationParser {
    public List<Field> getAnnotatedFields(Class clazz,Class annotation){
        List<Field> annotatedFields = new ArrayList<Field>();
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            if(isAnnotationPresent(annotation, field))
                annotatedFields.add(field);
        }
        return annotatedFields;
    }

    private boolean isAnnotationPresent(Class annotation, Field field) {
        return field.isAnnotationPresent(annotation);
    }
}
