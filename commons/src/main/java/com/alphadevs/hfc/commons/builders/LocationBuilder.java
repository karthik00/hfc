package com.alphadevs.hfc.commons.builders;

import com.alphadevs.hfc.commons.models.Location;

public class LocationBuilder {

    private Location location = new Location();

    public LocationBuilder withDefaults(){
        location.setAddress("address");
        return this;
    }

    public Location build(){
        return location;
    }

    public LocationBuilder withAddress(String address){
        location.setAddress(address);
        return this;
    }
}
