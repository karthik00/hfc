package com.alphadevs.hfc.commons;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;

/**
 * Created with IntelliJ IDEA.
 * User: karthik
 * Date: 17/01/13
 * Time: 11:53 PM
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class AppConfig {
    public @Bean
    TextEncryptor textEncryptor() {
        return Encryptors.noOpText();
    }
}
