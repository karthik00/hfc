package com.alphadevs.hfc.commons.models.updates.processors;

import com.mongodb.DBObject;

/**
 * Created with IntelliJ IDEA.
 * User: sreehari
 * Date: 04/02/13
 * Time: 21:38
 * To change this template use File | Settings | File Templates.
 */
public interface UpdateProcessor<E> {
    public void processBeforeSaveEvent(E source, DBObject dbObject);
}
