package com.alphadevs.hfc.commons.builders;

import com.alphadevs.hfc.commons.models.SessionToken;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: sravank
 * Date: 24/01/13
 * Time: 8:57 AM
 */
public class SessionTokenBuilder {

	private SessionToken sessionToken = new SessionToken();

	public SessionTokenBuilder withDefaults(){
		sessionToken.setUserAccount(new UserAccountBuilder().withDefaults().build());
		sessionToken.setId("id");
		sessionToken.setStatus(SessionToken.SessionStatus.VALID);
		sessionToken.setCreationDate(new Date());
		sessionToken.setUserAgent("UserAgent");
		return this;
	}

	public SessionToken build(){
		return sessionToken;
	}

	public SessionTokenBuilder withId(String tokenId) {
		sessionToken.setId(tokenId);
		return this;
	}
}
