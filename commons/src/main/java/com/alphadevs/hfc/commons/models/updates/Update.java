package com.alphadevs.hfc.commons.models.updates;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created with IntelliJ IDEA.
 * User: sreehari
 * Date: 04/02/13
 * Time: 00:04
 * To change this template use File | Settings | File Templates.
 */
@Document
public class Update {
    Event event;
    String type;

    Update(Event event,String type){
       this.event=event;
       this.type=type;
    }
}
