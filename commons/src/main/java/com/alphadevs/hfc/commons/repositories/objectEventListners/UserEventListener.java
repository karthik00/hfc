package com.alphadevs.hfc.commons.repositories.objectEventListners;

import com.alphadevs.hfc.commons.models.UserAccount;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UserEventListener extends AbstractMongoEventListener<UserAccount>{

    @Override
    public void onBeforeConvert(UserAccount source) {
        if (source.getCreationDate() == null)
            source.setCreationDate(new Date());
        super.onBeforeConvert(source);
    }
}

