package com.alphadevs.hfc.commons.repositories.objectEventListners;


import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.models.updates.processors.*;
import com.mongodb.DBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.stereotype.Component;

@Component
public class NGOListener extends AbstractMongoEventListener<NGO> {
    @Autowired
    UpdateProcessorFactory updateProcessorFactory;
    @Override
    public void onBeforeSave(NGO source, DBObject dbo) {
        for (UpdateProcessor<NGO> ngoProcessor : updateProcessorFactory.getNGOUpdateProcessors()){
                ngoProcessor.processBeforeSaveEvent(source,dbo);
        }
    }


}

