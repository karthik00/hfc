package com.alphadevs.hfc.commons.builders;

import com.alphadevs.hfc.commons.models.FieldOfAction;
import com.alphadevs.hfc.commons.models.Location;
import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.models.Requirement;

import java.util.ArrayList;
import java.util.List;

public class NGOBuilder {

    private NGO ngo = new NGO();

    public NGOBuilder withDefaults(){
        ngo.setName("NGO");
        ngo.setDescription("description");
        return this;
    }

    public NGOBuilder withName(String name){
        ngo.setName(name);
        return this;
    }

	public NGOBuilder withDescription(String description){
        ngo.setDescription(description);
        return this;
    }

	public NGOBuilder withVision(String vision){
        ngo.setVision(vision);
        return this;
    }

	public NGOBuilder withMission(String mission){
        ngo.setName(mission);
        return this;
    }

	public NGOBuilder withLocations(List<Location> locations){
        ngo.setLocations(locations);
        return this;
    }

    public NGOBuilder withFieldsOfAction(List<FieldOfAction> fieldsOfAction){
        ngo.setFieldsOfAction(fieldsOfAction);
        return this;
    }

    public NGO build(){
        return ngo;
    }

    public NGOBuilder withRequirements(List<Requirement> requirements) {
        ngo.setRequirements(requirements);
        return this;
    }
}
