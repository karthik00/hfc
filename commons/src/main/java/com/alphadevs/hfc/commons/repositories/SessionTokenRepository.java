package com.alphadevs.hfc.commons.repositories;

import com.alphadevs.hfc.commons.models.SessionToken;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionTokenRepository extends MongoRepository<SessionToken,String> {
    @Query("{ 'id' : ?0, 'status':'VALID' }")
    SessionToken findValidSessionById(String objectId);
}
