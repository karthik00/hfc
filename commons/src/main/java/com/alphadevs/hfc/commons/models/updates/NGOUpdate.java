package com.alphadevs.hfc.commons.models.updates;

import com.alphadevs.hfc.commons.models.NGO;

/**
 * Created with IntelliJ IDEA.
 * User: sreehari
 * Date: 01/02/13
 * Time: 21:57
 * To change this template use File | Settings | File Templates.
 */
public class NGOUpdate extends Update{
    private final NGO ngo;

    public NGOUpdate(NGO ngo , Event event,String type){
        super(event, type);
        this.ngo = ngo;
    }
}
