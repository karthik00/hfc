package com.alphadevs.hfc.commons.repositories;

import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.models.UserAccount;
import com.alphadevs.hfc.commons.models.follow.NGOFollowerDetail;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NGOFollowerRepository extends MongoRepository<NGOFollowerDetail, ObjectId> {
    Iterable<NGOFollowerDetail> findByFollowingUserId(ObjectId userAccountId);
    Iterable<NGOFollowerDetail> findByFollowedNGOId(ObjectId ngoId);
}