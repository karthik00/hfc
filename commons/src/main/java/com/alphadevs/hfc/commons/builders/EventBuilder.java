package com.alphadevs.hfc.commons.builders;

import com.alphadevs.hfc.commons.models.*;

import java.util.List;

public class EventBuilder {

    private Event event = new Event();

    public EventBuilder withDefaults(){
        event.setName("event");
        event.setDescription("description");
        return this;
    }

    public Event build(){
        return event;
    }

    public EventBuilder withName(String name){
        event.setName(name);
        return this;
    }

    public EventBuilder withDescription(String description){
        event.setDescription(description);
        return this;
    }

    public EventBuilder withFieldsOfAction(List<FieldOfAction> fieldsOfAction){
        event.setFieldsOfAction(fieldsOfAction);
        return this;
    }

    public EventBuilder withRequirements(List<Requirement> requirements) {
        event.setRequirements(requirements);
        return this;
    }

    public EventBuilder  withLocations(List<Location> locations){
        event.setLocations(locations);
     return this;
    }

	public EventBuilder withSponsoringNGO(NGO ngo){
		event.setSponsoringNGO(ngo);
		return this;
	}
}
