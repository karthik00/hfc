package com.alphadevs.hfc.commons.utils;

import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

public class MongoUtils {
	public static List<ObjectId> GetObjectIds(List<String> fieldsOfActionIds) {
		List<ObjectId> objectIds = new ArrayList<ObjectId>();
		for (String fieldsOfActionId : fieldsOfActionIds) {
			objectIds.add(new ObjectId(fieldsOfActionId));
		}
		return objectIds;
	}
}