package com.alphadevs.hfc.commons.models;

import com.alphadevs.hfc.commons.models.updates.ObservedField;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class NGO {

    @Id
    private ObjectId id;
    @ObservedField
    private String name;
    private String vision;
    @ObservedField
    private String mission;

    @DBRef
    private List<Location> locations;

    @DBRef
    private List<FieldOfAction> fieldsOfAction;

    @DBRef
    private List<Requirement> requirements;

    private String description;

    public NGO() {

    }

    public NGO(String name, String vision, String mission, String description) {
        this.name = name;
        this.vision = vision;
        this.mission = mission;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVision() {
        return vision;
    }

    public void setVision(String vision) {
        this.vision = vision;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FieldOfAction> getFieldsOfAction() {
        return fieldsOfAction;
    }

    public void setFieldsOfAction(List<FieldOfAction> fieldsOfAction) {
        this.fieldsOfAction = fieldsOfAction;
    }



    public String getId() {
        return id.toString();
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).
                append("name", name).
                append("vision", vision).
                append("mission", mission).
                append("description", description).
                append("fieldsOfAction", fieldsOfAction).
                toString();
    }

    public List<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<Requirement> requirements) {
        this.requirements = requirements;
    }

    @Override
    public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this,o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }
}