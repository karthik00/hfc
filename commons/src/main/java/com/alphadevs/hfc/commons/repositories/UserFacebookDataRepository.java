package com.alphadevs.hfc.commons.repositories;


import com.alphadevs.hfc.commons.models.UserFacebookData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UserFacebookDataRepository extends MongoRepository<UserFacebookData,String> {
    UserFacebookData findByFacebookId(String facebookId);
}
