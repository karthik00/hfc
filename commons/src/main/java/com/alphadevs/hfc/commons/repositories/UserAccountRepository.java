package com.alphadevs.hfc.commons.repositories;

import com.alphadevs.hfc.commons.models.UserAccount;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UserAccountRepository extends MongoRepository<UserAccount,String> {
    UserAccount findByEmailAndPassword(String email,String password);
    UserAccount findByFbId(String fbId);
}
