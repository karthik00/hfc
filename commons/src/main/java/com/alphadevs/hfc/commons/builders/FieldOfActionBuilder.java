package com.alphadevs.hfc.commons.builders;

import com.alphadevs.hfc.commons.models.FieldOfAction;

public class FieldOfActionBuilder {

    private FieldOfAction fieldOfAction = new FieldOfAction();

    public FieldOfActionBuilder withDefaults(){
        fieldOfAction.setName("default FieldOfAction");
        return this;
    }

    public FieldOfActionBuilder withName(String name){
        fieldOfAction.setName(name);
        return this;
    }

    public FieldOfActionBuilder withDescription(String description){
        fieldOfAction.setDescription(description);
        return this;
    }

    public FieldOfAction build(){
        return fieldOfAction;
    }

}
