package com.alphadevs.hfc.commons.repositories.objectEventListners;

import com.alphadevs.hfc.commons.models.SessionToken;
import com.alphadevs.hfc.commons.repositories.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SessionTokenEventListener extends AbstractMongoEventListener<SessionToken>{

    @Autowired
    UserAccountRepository userAccountRepository;

    @Override
    public void onBeforeConvert(SessionToken source) {
        Date date = new Date();

        if (source.getCreationDate() == null) {
            source.setCreationDate(date);
            source.getUserAccount().setLastLoggedInDate(date);
            userAccountRepository.save(source.getUserAccount());
        }
        super.onBeforeConvert(source);
    }
}

