package com.alphadevs.hfc.commons.repositories;

import com.alphadevs.hfc.commons.models.Event;
import com.alphadevs.hfc.commons.models.NGO;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends MongoRepository<Event,ObjectId>{
	@Query("{$or:[{'fieldsOfAction.$id':{$in:?0}},{'requirements.$id':{$in:?1}}]}")
	List<Event> searchEventsByFieldOfActionsAndRequirements(List<ObjectId> fieldsOfActionIds, List<ObjectId> requirementIds);

	@Query("{$or:[{'fieldsOfAction.$id':{$in:?0}},{'requirements.$id':{$in:?1}}]}")
	Page<Event> searchEventsByFieldOfActionsAndRequirements(List<ObjectId> fieldsOfActionIds, List<ObjectId> requirementIds, Pageable pageable);

    Page<Event> findEventsByNGOId(ObjectId ngoId);
}
