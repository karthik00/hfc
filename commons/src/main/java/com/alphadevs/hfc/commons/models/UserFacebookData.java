package com.alphadevs.hfc.commons.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.social.facebook.api.FacebookProfile;

@Document
public class UserFacebookData {
    @Id
    private String id;

    private String facebookId;

    @DBRef
    private UserAccount userAccount;

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    private FacebookProfile facebookProfile;

    public UserFacebookData(FacebookProfile facebookProfile) {
        this.facebookProfile = facebookProfile;
    }

    public UserFacebookData() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserFacebookData that = (UserFacebookData) o;

        if (facebookId != null ? !facebookId.equals(that.facebookId) : that.facebookId != null) return false;
        if (facebookProfile != null ? !facebookProfile.equals(that.facebookProfile) : that.facebookProfile != null)
            return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (userAccount != null ? !userAccount.equals(that.userAccount) : that.userAccount != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (facebookId != null ? facebookId.hashCode() : 0);
        result = 31 * result + (userAccount != null ? userAccount.hashCode() : 0);
        result = 31 * result + (facebookProfile != null ? facebookProfile.hashCode() : 0);
        return result;
    }
}