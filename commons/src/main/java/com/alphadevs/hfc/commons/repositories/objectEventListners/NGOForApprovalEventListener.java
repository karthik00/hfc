package com.alphadevs.hfc.commons.repositories.objectEventListners;

import com.alphadevs.hfc.commons.models.NGOForApproval;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class NGOForApprovalEventListener extends AbstractMongoEventListener<NGOForApproval>{
    @Override
    public void onBeforeConvert(NGOForApproval source) {
        if (source.getCreationDate()==null)
            source.setCreationDate(new Date());
        else
            source.setLastUpdatedDate(new Date());
        super.onBeforeConvert(source);
    }
}
