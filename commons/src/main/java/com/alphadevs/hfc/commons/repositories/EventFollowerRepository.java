package com.alphadevs.hfc.commons.repositories;

import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.models.UserAccount;
import com.alphadevs.hfc.commons.models.follow.EventFollowerDetail;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventFollowerRepository extends MongoRepository<EventFollowerDetail, ObjectId> {
    Iterable<NGO> fetchUserFollowingEvents(ObjectId userId);
    Iterable<UserAccount> fetchUsersFollowingEvent(ObjectId eventId);
}