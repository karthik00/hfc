package com.alphadevs.hfc.commons.models.follow;

import com.alphadevs.hfc.commons.models.Event;
import com.alphadevs.hfc.commons.models.UserAccount;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class EventFollowerDetail extends FollowerDetail {

    @Id
    private ObjectId id;

    @DBRef
    private Event followingEvent;

    public EventFollowerDetail(UserAccount followingUser, Event followingEvent) {
        super(followingUser);
        this.followingEvent=followingEvent;
    }

    public Event getFollowingEvent() {
        return followingEvent;
    }
    public void setFollowingEvent(Event followingEvent) {
        this.followingEvent = followingEvent;
    }
}
