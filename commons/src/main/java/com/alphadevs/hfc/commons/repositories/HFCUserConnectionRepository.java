package com.alphadevs.hfc.commons.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.mongo.ConnectionService;
import org.springframework.social.connect.mongo.MongoUsersConnectionRepository;
import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA.
 * User: karthik
 * Date: 18/01/13
 * Time: 12:00 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class HFCUserConnectionRepository extends MongoUsersConnectionRepository{

    @Autowired
    public HFCUserConnectionRepository(ConnectionService mongoService, ConnectionFactoryLocator connectionFactoryLocator, TextEncryptor textEncryptor) {
        super(mongoService, connectionFactoryLocator, textEncryptor);
//        super.setConnectionSignUp(new ConnectionSignUp() {
//            public String execute(Connection<?> connection) {
//                return connection.getDisplayName();
//            }
//        });
    }
}