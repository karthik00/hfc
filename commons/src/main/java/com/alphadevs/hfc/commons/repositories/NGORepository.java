package com.alphadevs.hfc.commons.repositories;

import com.alphadevs.hfc.commons.models.NGO;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NGORepository  extends MongoRepository<NGO, ObjectId> {
    @Query("{$or:[{'fieldsOfAction.$id':{$in:?0}},{'requirements.$id':{$in:?1}}]}")
    List<NGO> searchNGOsByFieldOfActionsAndRequirements(List<ObjectId> fieldsOfActionIds, List<ObjectId> requirementIds);

    @Query("{$or:[{'fieldsOfAction.$id':{$in:?0}},{'requirements.$id':{$in:?1}}]}")
    Page<NGO> searchNGOsByFieldOfActionsAndRequirements(List<ObjectId> fieldsOfActionIds, List<ObjectId> requirementIds, Pageable pageable);
}