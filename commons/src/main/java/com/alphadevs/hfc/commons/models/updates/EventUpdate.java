package com.alphadevs.hfc.commons.models.updates;

public class EventUpdate extends Update{
    private final Event event;

    public EventUpdate(Event event , Event updateEvent,String type){
        super(updateEvent, type);
        this.event=event;
    }
}
