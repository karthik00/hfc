package com.alphadevs.hfc.dbseeding;

import com.alphadevs.hfc.commons.models.NGO;
import com.alphadevs.hfc.commons.repositories.NGORepository;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NGOsSeeding {

    static Logger log = Logger.getLogger(NGOsSeeding.class);

    @Autowired
    NGORepository ngoRepository;

    public void seed(JSONArray jsonArray) throws JSONException {
        log.setLevel(Level.INFO);
        log.info("clearing existing ngos");
        ngoRepository.deleteAll();
        for(int i=0; i<jsonArray.length();i++){
            JSONObject ngoData= jsonArray.getJSONObject(i);
            NGO ngo=new NGO();
            ngo.setName(ngoData.getString("name"));
            ngo.setVision(ngoData.getString("vision"));
            ngo.setMission(ngoData.getString("mission"));
            ngoRepository.save(ngo);
        }
        log.info("ngos seeding done");
    }
}
