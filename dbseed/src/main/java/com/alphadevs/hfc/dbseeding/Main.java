package com.alphadevs.hfc.dbseeding;


import com.alphadevs.hfc.commons.builders.*;
import com.alphadevs.hfc.commons.models.*;
import com.alphadevs.hfc.commons.repositories.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static Logger log = Logger.getLogger(Main.class);

    private Resource dbResource;

    public static void main(String[] args) throws IOException, JSONException {
        log.setLevel(Level.INFO);
		//load all beans and Autowire then.
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:app.xml");

		//get all repositories
		EventRepository eventRepository = context.getBean(EventRepository.class);
		FieldOfActionRepository fieldOfActionRepository = context.getBean(FieldOfActionRepository.class);
		HFCUserConnectionRepository hfcUserConnectionRepository = context.getBean(HFCUserConnectionRepository.class);
		LocationRepository locationRepository = context.getBean(LocationRepository.class);
		NGOForApprovalRepository ngoForApprovalRepository = context.getBean(NGOForApprovalRepository.class);
		NGORepository ngoRepository = context.getBean(NGORepository.class);
		RequirementsRepository requirementsRepostiory = context.getBean(RequirementsRepository.class);
		SessionTokenRepository sessionTokenRepository = context.getBean(SessionTokenRepository.class);
		UserAccountRepository userAccountRepository = context.getBean(UserAccountRepository.class);
		UserFacebookDataRepository userFacebookDataRepository = context.getBean(UserFacebookDataRepository.class);

		ClearDatabase(eventRepository, fieldOfActionRepository, locationRepository, ngoRepository, requirementsRepostiory);

		Location bangalore = new LocationBuilder().withAddress("Bangalore").build();
		Location hyderabad = new LocationBuilder().withAddress("Hyderabad").build();
		locationRepository.save(bangalore);
		locationRepository.save(hyderabad);

		FieldOfAction childEducation = new FieldOfActionBuilder().withName("Child Education").withDescription("Provide Education For Every Child").build();
		FieldOfAction womenWelfare = new FieldOfActionBuilder().withName("Women Welfare").withDescription("Taking care of helpless women").build();
		fieldOfActionRepository.save(childEducation);
		fieldOfActionRepository.save(womenWelfare);

		Requirement mathsTeacher = new RequirementBuilder().withName("Maths Teacher").
				withDescription("Permanet Teacher").build();
		Requirement careTakerForWomen = new RequirementBuilder().withName("A Women Care Taker").
				withDescription("A Women Care Taker").build();

		Requirement musician = new RequirementBuilder().withName("Musician").
				withDescription("Musician For a Night").build();

		requirementsRepostiory.save(mathsTeacher);
		requirementsRepostiory.save(careTakerForWomen);
		requirementsRepostiory.save(musician);

		NGO akshayaPatra = new NGOBuilder()
				.withName("Akshaya Patra")
				.withDescription("Providing Food and Education for all")
				.withFieldsOfAction(Arrays.asList(childEducation))
				.withRequirements(Arrays.asList(mathsTeacher))
				.withLocations(Arrays.asList(hyderabad, bangalore))
				.build();
		NGO motherTheresaFoudation = new NGOBuilder()
				.withName("Mother Theresa Foundation")
				.withDescription("Care taking of Women and Children")
				.withFieldsOfAction(Arrays.asList(womenWelfare,childEducation))
				.withRequirements(Arrays.asList(careTakerForWomen))
				.withLocations(Arrays.asList(bangalore))
				.build();

		ngoRepository.save(akshayaPatra);
		ngoRepository.save(motherTheresaFoudation);

		Event january1stCelebrationsAtAkshayPatra = new EventBuilder().withName("January1stCelebrations")
				.withLocations(Arrays.asList(bangalore, hyderabad))
				.withDescription("On the occasion of January 1st")
				.withRequirements(Arrays.asList(musician))
				.withFieldsOfAction(Arrays.asList(childEducation))
				.withSponsoringNGO(akshayaPatra)
				.build();
		Event sankrantiCelebrationsAtMotherTheresaFoundation = new EventBuilder().withName("Sankranthi Celebrations")
				.withLocations(Arrays.asList(bangalore))
				.withDescription("Sankranthi Celebrations")
				.withRequirements(Arrays.asList(musician))
				.withFieldsOfAction(Arrays.asList(womenWelfare))
				.withSponsoringNGO(akshayaPatra)
				.build();
		eventRepository.save(january1stCelebrationsAtAkshayPatra);
		eventRepository.save(sankrantiCelebrationsAtMotherTheresaFoundation);
	}

	private static void ClearDatabase(MongoRepository... mongoRepositories) {
		for (MongoRepository mongoRepository : mongoRepositories) {
			mongoRepository.deleteAll();
		}
	}
}
