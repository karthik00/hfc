package com.alphadevs.hfc.dbseeding;

import com.alphadevs.hfc.commons.models.UserAccount;
import com.alphadevs.hfc.commons.repositories.SessionTokenRepository;
import com.alphadevs.hfc.commons.repositories.UserAccountRepository;
import com.alphadevs.hfc.services.auth.AuthenticationServiceImpl;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UsersSeeding {
    static Logger log = Logger.getLogger(UsersSeeding.class);
    @Autowired
    AuthenticationServiceImpl authenticationService;
    @Autowired
    UserAccountRepository userAccountRepository;
    @Autowired
    SessionTokenRepository sessionTokenRepository;

    public void seed(JSONArray jsonArray) throws JSONException {
        log.setLevel(Level.INFO);
        log.info("clearing existing user info");
        userAccountRepository.deleteAll();
        sessionTokenRepository.deleteAll();
        for(int i=0; i<jsonArray.length();i++){
            JSONObject userData= jsonArray.getJSONObject(i);
            UserAccount user=new UserAccount();
            user.setEmail(userData.getString("email"));
            user.setPassword("password");
            authenticationService.registerUser(user);
        }
        log.info("users seeding done");
    }
}
